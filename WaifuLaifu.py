import configparser
import pymongo
import urllib.request
import json
import re
import bson
import uuid
import random
import textwrap
from enum import Enum
from vndb import VNDB
from secret import *
from extensions import RegularExtensions, draw_trade, draw_fight


class WaifuLaifu:

    def __init__(self, server=None, user=None, persistent=False):
        self.client = pymongo.MongoClient(host=db_ip, port=27017, username=db_user, password=db_pw)
        self.user_db = self.client.waifulaifu.users
        self.server_db = self.client.waifulaifu.servers
        self.character_db = self.client.waifulaifu.characters
        self.base_stat_db = self.client.waifulaifu.base_stats
        self.trade_db = self.client.waifulaifu.trades
        self.attack_db = self.client.waifulaifu.attacks
        self.element_db = self.client.waifulaifu.elements
        self.fight_db = self.client.waifulaifu.fights
        self.adventure_db = self.client.waifulaifu.adventures
        self.set_db = self.client.waifulaifu.sets
        self.item_db = self.client.waifulaifu.items

        if server is not None:
            self.server_id = server.id
            self.server_default_channel = self.get_default_channel(server)
            self.server_document = self.new_server_check()
        else:
            self.server_document = None
        if user is not None:
            self.user_id = user.id
            self.user_name = user.display_name
            self.user_document = self.new_user_check()
            self.user_team = self.user_document['team']
            self.gems = self.user_document['collection']['gems']
        else:
            self.user_document = None

        self.config_data = configparser.ConfigParser()
        self.config_data.read(Path("./data/waifulaifu.ini"))
        self.persistent = persistent

    def close(self, override=False):
        if self.persistent:
            if override:
                self.client.close()
        else:
            self.client.close()

    def refresh(self):
        if self.user_id is not None:
            self.user_document = self.new_user_check()
        if self.server_id is not None:
            self.server_document = self.new_server_check()

    #########################
    #        GENERAL        #
    #########################

    def new_server_check(self):
        doc = self.server_db.find_one({'serverid': self.server_id})
        if doc is None:
            i = {
                'serverid': self.server_id,
                'current_spawns': [],
                'shop': list(self.item_db.aggregate([{'$sample': {'size': 5}}])),
                'settings': {
                    'level_up_message': True,
                    'command_prefix': self.config_data['WaifuLaifu']['DefaultPrefix'],
                    'spawn_channel': self.server_default_channel
                }
            }
            self.server_db.insert(i)
            doc = self.server_db.find_one({'serverid': self.server_id})
        return doc

    def new_user_check(self):
        doc = self.user_db.find_one({'userid': self.user_id})
        if doc is None:
            i = {
                'userid': self.user_id,
                'profile': {
                    'quote': "",
                    'profile_name': f"{self.user_name}"
                },
                'collection': {
                    'waifus': [],
                    'wishlist': [],
                    'favorites': [],
                    'nendos': [],
                    'items': [],
                    'boosters': [],
                    'gems': 0
                },
                'team': [],
                'market': [],
                'settings': {
                    'wishlist_public': False,
                    'inventory_public': False,
                    'favorites_public': False,
                    'adventure_log': False
                },
                'premium': 0
            }
            self.user_db.insert(i)
            doc = self.user_db.find_one({'userid': self.user_id})
        return doc

    def profile_check(self, user):
        return True if self.user_db.find_one({'userid': user.id}) is not None else False

    def load_user(self, user):
        self.user_id = user.id
        self.user_name = user.display_name
        self.user_document = self.new_user_check()
        self.user_team = self.user_document['team']
        self.gems = self.user_document['collection']['gems']

    @staticmethod
    def get_default_channel(guild):
        for channel in guild.text_channels:
            if channel.permissions_for(guild.me).send_messages:
                return channel.name
        return None

    def check_setting(self, data):
        return self.user_document['settings'][data]

    def switch_setting(self, key, value):
        if self.user_document is not None and key in self.user_document['settings'].keys():
            self.user_db.find_one_and_update({'userid': self.user_id}, {'$set': {f'settings.{key}': value}})
        if self.server_document is not None and key in self.server_document['settings'].keys():
            self.server_db.find_one_and_update({'serverid': self.server_id}, {'$set': {f'settings.{key}': value}})
        self.refresh()

    def get_series_data(self, series_id, site="mal"):
        counter = 0
        character_ids = []
        if site == "mal":
            with urllib.request.urlopen(f"https://api.jikan.moe/v3/anime/{series_id}/characters_staff") as url:
                data = json.loads(url.read().decode())
            for character in data['characters']:
                character_ids.append(int(character['mal_id']))
                result = self.get_character_data(character_id=character['mal_id'], site="mal")
                if result:
                    counter += 1

            with urllib.request.urlopen(f"https://api.jikan.moe/v3/anime/{series_id}") as url:
                series_data = json.loads(url.read().decode())
            set_data = self.set_db.find_one({'set_name': series_data['title']})
            if set_data is None:
                i = {
                    'set_name': series_data['title'],
                    'set_image': series_data['image_url'],
                    'characters': character_ids,
                    'price': 0
                }
                self.set_db.insert(i)
            else:
                for character in data['characters']:
                    if character['mal_id'] not in set_data['characters']:
                        set_data = self.set_db.find_one_and_update({'set_name': series_data['title']}, {'$push': {'characters': character['mal_id']}}, return_document=pymongo.ReturnDocument.AFTER)

        elif site == "vndb":
            vndb_connection = VNDB(username=vndb_user, password=vndb_password)
            data = vndb_connection.get_characters(novel_id=series_id)
            novel_data = vndb_connection.get_novel(data['items'][0]['vns'][0][0])

            for character in data['items']:
                spoiler = False
                for vn in character['vns']:
                    if vn[1] > 0:
                        spoiler = True
                        break
                if not spoiler:
                    character_ids.append(int(character['id']))
                    result = self.get_character_data(character_id=character['id'], site="vndb", novel_data=novel_data)
                    if result:
                        counter += 1

            set_data = self.set_db.find_one({'set_name': novel_data['items'][0]['title']})
            if set_data is None:
                i = {
                    'set_name': novel_data['items'][0]['title'],
                    'set_image': novel_data['items'][0]['image'],
                    'characters': character_ids,
                    'price': 0
                }
                self.set_db.insert(i)
            else:
                for character in character_ids:
                    if character not in set_data['characters']:
                        set_data = self.set_db.find_one_and_update({'set_name': novel_data['items'][0]['title']}, {'$push': {'characters': character}}, return_document=pymongo.ReturnDocument.AFTER)

            vndb_connection.close()

        return counter

    def get_character_data(self, character_id: int, site="mal", novel_data=None):
        doc = self.character_db.find_one({'id': character_id})
        if doc is not None:
            return False, None

        if site == "mal":
            with urllib.request.urlopen(f"https://api.jikan.moe/v3/character/{character_id}") as url:
                character_data = json.loads(url.read().decode())
                i = {
                    'id': character_id,
                    'name': character_data['name'],
                    'origin': character_data['animeography'][0]['name'],
                    'origin_site': "mal",
                    'src': character_data['url'],
                    'description': textwrap.wrap(character_data['about'].replace("\r\n", "$$"), 1021)[0] + "...",
                    'gender': "f",
                    'picture': character_data['image_url'],
                    'wishlist': 0,
                    'favorites': 0
                }
                self.character_db.insert(i)
                return True, character_data['name']
        elif site == "vndb":
            vndb_connection = VNDB(username=vndb_user, password=vndb_password)
            data = vndb_connection.get_character(character_id=character_id)
            if novel_data is None:
                novel_data = vndb_connection.get_novel(data['items'][0]['vns'][0][0])
            if data['items'][0]['description'] is None:
                data['items'][0]['description'] = ""
            vndb_connection.close()
            i = {
                'id': character_id,
                'name': data['items'][0]['name'],
                'origin': novel_data['items'][0]['title'],
                'origin_site': "vndb",
                'src': f"https://vndb.org/c{character_id}",
                'description': textwrap.wrap(data['items'][0]['description'].replace("\n", "$"), 1021)[0] + "...",
                'gender': data['items'][0]['gender'],
                'picture': data['items'][0]['image'],
                'wishlist': 0,
                'favorites': 0
            }
            self.character_db.insert(i)
            return True, data['items'][0]['name']

    def get_character(self, character_id: int):
        return self.character_db.find_one({'id': character_id})

    def get_attack(self, attack_uid):
        return self.attack_db.find_one({'uid': attack_uid})

    def get_id_list(self):
        ids = []
        for character in self.user_document['collection']['waifus']:
            ids.append(character['id'])
        return ids

    def remove_character(self, local_id: int, user=None):
        if user is None:
            user = self.user_document
        else:
            user = self.user_db.find_one({'userid': user})
        self.user_db.find_one_and_update({'userid': user['userid']}, {'$pull': {'collection.waifus': {'local_id': local_id}}})
        self.user_db.update_many({'userid': user['userid'], 'collection.waifus.local_id': {'$gt': local_id}}, {'$inc': {'collection.waifus.$[].local_id': -1}})

    #########################
    #     USER INVENTORY    #
    #########################

    def get_local_character(self, local_id, doc=None):
        if doc is None:
            doc = self.user_document
        for character in doc['collection']['waifus']:
            if character['local_id'] == local_id:
                return character
        return None

    def set_owned(self, character_list):
        owned_list = []
        for character in self.user_document['collection']['waifus']:
            owned_list.append(character['id'])

        for character in character_list:
            if character['id'] in owned_list:
                character['owned'] = True
            else:
                character['owned'] = False

        return character_list

    def get_filters(self, data):
        filter_data = {}
        order_data = {'reversed': False, 'page': 1, 'sort_by': []}
        for element in data:
            if element.startswith(("-p", "-page")):
                page = RegularExtensions.intTryParse(element.replace("-p ", ""))
                if page is not None and page > 1:
                    order_data['page'] = page
            elif element.startswith(("-s", "-series")):
                filter_data['origin'] = element.replace("-s ", "").lower()
            elif element.startswith(("-c", "-n", "-name", "-character")):
                filter_data['name'] = element.replace("-c ", "").lower()
            elif element.startswith(("-o", "-order")):
                element = element.replace("-o ", "").split(" ")
                for element_part in element:
                    if element_part.lower() in ["asc", "ascending"]:
                        order_data['reversed'] = False
                    elif element_part.lower() in ["desc", "descending"]:
                        order_data['reversed'] = True
                    elif element_part.lower() in ["fav", "favorite", "w", "wishlist", "atk", "def", "hp", "affection", "rarity", "id"]:
                        order_data["sort_by"].append(element_part)
            else:
                char = self.character_db.find_one({'name': bson.regex.Regex(f"^{element}|{element}$", flags=re.IGNORECASE)})
                if char is not None:
                    filter_data['name'] = element
                else:
                    series = self.character_db.find_one({'origin': bson.regex.Regex(f"^{element}|{element}$", flags=re.IGNORECASE)})
                    if series is not None:
                        filter_data['origin'] = element
        return filter_data, order_data

    def character_checker(self, characters, filters, order_data):
        character_list = []
        for character in characters:
            append = True
            character_doc = self.character_db.find_one({'id': character['id']})
            for key, value in filters.items():
                if key is "name":
                    if not re.search(rf"^({value}|{RegularExtensions.reverse_words(value)})|({value}|{RegularExtensions.reverse_words(value)})$", character_doc['name'], flags=re.IGNORECASE):
                        append = False
                elif key is "origin":
                    if not re.search(rf"^{value}|{value}$", character_doc['origin'], flags=re.IGNORECASE):
                        append = False
            if append:
                character['local_id'] = character['local_id']
                character['name'] = character_doc['name']
                character['origin'] = character_doc['origin']
                character_list.append(character)

        character_list = self.set_owned(character_list)

        def getKey(item):
            sort_parameters = []
            for sort_parameter in order_data['sort_by']:
                if sort_parameter.lower() in ["id"]:
                    sort_parameters.append(item['id'])
                elif sort_parameter.lower() in ["atk", "def", "hp"]:
                    sort_parameters.append(item[sort_parameter.upper()])
                elif sort_parameter.lower() in ["affection", "rarity"]:
                    sort_parameters.append(item[sort_parameter.title()])

            if len(sort_parameters) == 0:
                sort_parameters.append(item['id'])

            return sort_parameters

        return sorted(character_list, key=getKey, reverse=order_data['reversed'])

    def db_sorter(self, filter_data, order_data):
        if order_data['reversed']:
            reverse = pymongo.DESCENDING
        else:
            reverse = pymongo.ASCENDING

        filters = {}
        if "name" in filter_data:
            filters['name'] = bson.regex.Regex(f"^({filter_data['name']}|{RegularExtensions.reverse_words(filter_data['name'])})|({filter_data['name']}|{RegularExtensions.reverse_words(filter_data['name'])})$", flags=re.IGNORECASE)
        if "origin" in filter_data:
            filters['origin'] = bson.regex.Regex(f"^{filter_data['origin']}|{filter_data['origin']}$", flags=re.IGNORECASE)

        sort_parameters = []
        for sort_parameter in order_data['sort_by']:
            if sort_parameter.lower() in ["id"]:
                sort_parameters.append(("id", reverse))
            if sort_parameter.lower() in ["fav", "favorite"]:
                sort_parameters.append(("favorites", reverse))
            elif sort_parameter.lower() in ["wishlist"]:
                sort_parameters.append(("wishlist", reverse))

        if len(sort_parameters) == 0:
            sort_parameters.append(("id", reverse))

        character_list = list(self.character_db.find(filters).sort(sort_parameters))

        character_list = self.set_owned(character_list)

        return character_list

    def get_series_list(self, data):
        series_list = []
        for character in data:
            set_exists = False
            for series in series_list:
                if series['name'] == character['origin']:
                    set_exists = True
                    series['characters'].append(character)
            if not set_exists:
                set_doc = self.set_db.find_one({'set_name': character['origin']})
                if set_doc is not None:
                    series_list.append({'name': character['origin'], 'characters': [character], 'character_amount': len(set_doc['characters'])})
                else:
                    series_list.append({'name': character['origin'], 'characters': [character], 'character_amount': 0})
                continue
        return series_list

    def get_inventory(self, data):
        filter_data, order_data = self.get_filters(data)
        character_list = self.character_checker(self.user_document['collection']['waifus'], filters=filter_data, order_data=order_data)
        series_data = self.get_series_list(character_list)
        return character_list, series_data, filter_data, order_data

    def get_lookup(self, data):
        filter_data, order_data = self.get_filters(data)
        character_list = self.db_sorter(filter_data, order_data)
        series_data = self.get_series_list(character_list)
        return character_list, series_data, filter_data, order_data

    def get_view(self, local_id, user=None):
        user = self.user_document if user is None else self.user_db.find_one({'userid': user.id})
        if user is None:
            return False
        character = self.get_local_character(local_id, user)
        character_doc = None
        if character is not None:
            character_doc = self.character_db.find_one({'id': character['id']})
        return character, character_doc

    ####################################
    #        CHARACTER INVENTORY       #
    ####################################

    def level_up(self, doc, levels=1):
        for i in range(levels):
            doc['LVL'] = doc['LVL'] + 1
            doc['ATK'] = round(((2 * (doc['base_stats'][0] + doc['ATK']) * doc['LVL']) / 100) + 5, 0)
            doc['DEF'] = round(((2 * (doc['base_stats'][1] + doc['DEF']) * doc['LVL']) / 100) + 5, 0)
            doc['HP'] = round(((2 * (doc['base_stats'][2] + doc['HP']) * doc['LVL']) / 100) + doc['LVL'] + 10, 0)
        return doc

    def nickname_set(self, local_id, name):
        doc = self.user_db.find_one({'userid': self.user_id})
        if len(doc['collection']['waifus']) >= local_id:
            doc = self.user_db.find_one_and_update({'userid': self.user_id, 'collection.waifus.local_id': local_id}, {'$set': {'collection.waifus.$.nickname': name}}, return_document=pymongo.ReturnDocument.AFTER)
            return True, self.character_db.find_one({'id': doc['collection']['waifus'][local_id - 1]['id']})['name']
        return IndexError, None

    def nickname_reset(self, local_id):
        doc = self.user_db.find_one({'userid': self.user_id})
        if len(doc['collection']['waifus']) >= local_id:
            if doc['collection']['waifus'][local_id - 1]['nickname'] is not None:
                doc = self.user_db.find_one_and_update({'userid': self.user_id, 'collection.waifus.local_id': local_id}, {'$set': {'collection.waifus.$.nickname': None}}, return_document=pymongo.ReturnDocument.AFTER)
                return True, self.character_db.find_one({'id': doc['collection']['waifus'][local_id - 1]['id']})['name']
            return False, None
        return IndexError, None

    def team_check(self, uid, user=None):
        if user is None:
            user = self.user_document
        else:
            user = self.user_db.find_one({'userid': user})
        for waifu in user['team']:
            if waifu['uid'] == uid:
                return False
        return True

    def team_add(self, local_id):
        local_doc = self.get_local_character(local_id)
        if local_doc is None:
            return None

        name = self.get_character(local_doc['id'])['name']
        if not self.team_check(local_doc['uid']):
            return "Duplicate", [name, local_doc['nickname']]
        elif len(self.user_team) == 3:
            return "Full", [name, local_doc['nickname']]
        self.user_db.find_one_and_update({'userid': self.user_id}, {'$push': {'team': {'uid': local_doc['uid'], 'local_id': local_doc['local_id']}}})
        return [name, local_doc['nickname']]

    def team_remove(self, local_id, user=None):
        if user is None:
            user = self.user_document
        else:
            user = self.user_db.find_one({'userid': user})
        local_doc = self.get_local_character(local_id, user)
        if local_doc is None:
            return None

        name = self.get_character(local_doc['id'])['name']
        if self.team_check(local_doc['uid']):
            return False, [name, local_doc['nickname']]
        self.user_db.find_one_and_update({'userid': user['userid'], 'team.local_id': local_id}, {'$pull': {'team.$': None}})
        return True, [name, local_doc['nickname']]

    def team_get(self, user=None):
        if user is None:
            user = self.user_document
        else:
            user = self.user_db.find_one({'userid': user})
        team_list = []
        for waifu in user['team']:
            team_list.append(self.get_local_character(waifu['local_id']))
        return team_list

    def team_get_list(self):
        return self.user_team

    #########################
    #        TRADING        #
    #########################

    def trade_start(self, user):
        trade = {
            'uid': uuid.uuid4().hex,
            'u1': self.user_id,
            'u1_offer': [],
            'u1_ready': False,
            'u2': user.id,
            'u2_offer': [],
            'u2_ready': False,
            'accepted': False,
            'last_status': None
        }
        self.trade_db.insert(trade)
        return trade['uid']

    def trade_delete(self, trade_uid):
        self.trade_db.delete_one({'uid': trade_uid})

    def trade_accept(self, uid):
        trade = self.trade_get(trade_uid=uid)
        self.trade_db.find_one_and_update({'uid': trade['uid']}, {'$set': {'accepted': True}})

    def trade_add(self, index):
        trade = self.trade_get(user_id=self.user_id)
        if trade['u1'] == self.user_id:
            user = "u1"
        else:
            user = "u2"
        if len(trade[f'{user}_offer']) >= 10:
            return 10
        if len(self.user_document['collection']['waifus']) >= index-1:
            if not self.user_document['collection']['waifus'][index-1] in trade[f"{user}_offer"]:
                self.trade_db.find_one_and_update({'uid': trade['uid']}, {'$push': {f"{user}_offer": self.user_document['collection']['waifus'][index-1]}, '$set': {'u1_ready': False, 'u2_ready': False}})
                return True
            return False
        return IndexError

    def trade_remove(self, index):
        trade = self.trade_get(user_id=self.user_id)
        if trade['u1'] == self.user_id:
            user = "u1"
        else:
            user = "u2"
        if len(self.user_document['collection']['waifus']) >= index-1:
            if self.user_document['collection']['waifus'][index-1] in trade[f"{user}_offer"]:
                self.trade_db.find_one_and_update({'uid': trade['uid']}, {'$pull': {f"{user}_offer": self.user_document['collection']['waifus'][index-1]}, '$set': {'u1_ready': False, 'u2_ready': False}})
                return True
            return False
        return IndexError

    def trade_ready(self):
        trade = self.trade_get(user_id=self.user_id)
        user = "u1" if trade['u1'] == self.user_id else "u2"
        if not trade[f'{user}_ready']:
            trade = self.trade_db.find_one_and_update({'uid': trade['uid']}, {'$set': {f'{user}_ready': True}}, return_document=pymongo.ReturnDocument.AFTER)
            if trade['u1_ready'] and trade['u2_ready']:
                return "Complete"
            return True
        return False

    def trade_unready(self):
        trade = self.trade_get(user_id=self.user_id)
        user = "u1" if trade['u1'] == self.user_id else "u2"
        if trade[f'{user}_ready']:
            self.trade_db.find_one_and_update({'uid': trade['uid']}, {'$set': {f'{user}_ready': False}})
            return True
        return False

    def trade_complete(self):
        trade = self.trade_get(user_id=self.user_id)
        for character in trade['u1_offer']:
            self.remove_character(character['local_id'], user=trade['u1'])
            self.team_remove(character['local_id'], user=trade['u1'])
            character['local_id'] = len(self.user_db.find_one({'userid': trade['u2']})['collection']['waifus']) + 1
            character['available'] = True
            self.user_db.find_one_and_update({'userid': trade['u2']}, {'$push': {'collection.waifus': character}})
        for character in trade['u2_offer']:
            self.remove_character(character['local_id'], user=trade['u2'])
            self.team_remove(character['local_id'], user=trade['u2'])
            character['local_id'] = len(self.user_db.find_one({'userid': trade['u1']})['collection']['waifus']) + 1
            character['available'] = True
            self.user_db.find_one_and_update({'userid': trade['u1']}, {'$push': {'collection.waifus': character}})
        self.trade_db.delete_one({'uid': trade['uid']})

    def trade_get(self, trade_uid=None, user_id=None):
        if trade_uid is not None:
            trade = self.trade_db.find_one({'uid': trade_uid})
            return trade
        if user_id is not None:
            trade = self.trade_db.find_one({'u1': user_id, 'u2': self.user_id})
            return trade
        else:
            trade = self.trade_db.find_one({'$or': [{'u1': self.user_id}, {'u2': self.user_id}]})
            return trade

    def trade_get_img(self, uid, names):
        doc = self.trade_db.find_one({'uid': uid})
        lists = []
        for user in range(2):
            waifu_list = []
            for i in range(len(doc[f'user_{user+1}_offer'])):
                local_doc = doc[f'user_{user+1}_offer'][i]
                waifu_doc = self.get_character(local_doc['id'])
                name = f"{local_doc['nickname']} ({waifu_doc['name']})" if local_doc['nickname'] is not None else waifu_doc['name']
                waifu_list.append([f"{local_doc['local_id']}", f"{local_doc['id']}", name, f"LVL {local_doc['LVL']}", f"HP {local_doc['HP']}", f"ATK {local_doc['ATK']}", f"DEF {local_doc['DEF']}"])
            lists.append(waifu_list)

        edited_list, content_length = RegularExtensions.sbs_string_extender(lists, " | ", "\t")
        edited_string = '\n'.join(edited_list)

        string = f'''{names[0] + "'s Offer:" + " " * (content_length - len(names[0] + "'s Offer:"))}   {names[1] + "'s Offer:" + " " * (content_length - len(names[1] + "'s Offer:"))}\n\n{edited_string}'''
        image = draw_trade(string)
        return image

    def trade_update_status(self, uid, message_data):
        self.trade_db.find_one_and_update({'uid': uid}, {'$set': {'last_status': message_data}})

    #########################
    #        FIGHTING       #
    #########################

    # Manual #

    def fight_create(self, user, channel):
        user_2_doc = self.user_db.find_one({'userid': user.id})
        fight = {
            'uid': uuid.uuid4().hex,
            'channel': channel.id,
            'u1': self.user_id,
            'u1_team': self.team_get(),
            'u1_ready': False,
            'u1_current': 0,
            'u2': user.id,
            'u2_team': self.team_get(user_2_doc),
            'u2_ready': False,
            'u2_current': 0,
            'turn': random.choice(["u1", "u2"]),
            'accepted': False,
            'running': False,
            'last_status': None
        }
        self.fight_db.insert(fight)
        return fight['uid']

    def fight_delete(self, fight_uid):
        fight_doc = self.fight_get(fight_uid=fight_uid)
        for i in range(2):
            for character in fight_doc[f'user_{i}_team']:
                self.user_db.find_one_and_update({'userid': fight_doc[f'user_{i}'], 'collection.waifus.uid': character['uid']}, {'$set': {'collection.waifus.$.available': True}})
        self.fight_db.delete_one({'uid': fight_uid})

    def fight_accept(self, uid):
        fight = self.fight_get(fight_uid=uid)
        self.fight_db.find_one_and_update({'uid': fight['uid']}, {'$set': {'accepted': True}})
        for i in range(2):
            for character in fight[f'user_{i}_team']:
                if not character['available']:
                    self.fight_db.find_one_and_update({'uid': uid}, {'$pull': {f'user_{i}_team': character}})
                else:
                    self.user_db.find_one_and_update({'userid': fight[f'user_{i}'], 'collection.waifus.uid': character['uid']}, {'$set': {'collection.waifus.$.available': False}})
        return True

    def fight_add(self, index):
        fight = self.fight_get(user_id=self.user_id)
        user = "u1" if fight['u1'] == self.user_id else "u2"
        if len(fight[f'{user}_team']) >= 3:
            return 3
        
        if len(self.user_document['collection']['waifus']) < index - 1:
            return IndexError

        if self.user_document['collection']['waifus'][index - 1] in fight[f"{user}_team"]:
            return False

        character_doc = self.get_local_character(index)
        if character_doc['available']:
            self.fight_db.find_one_and_update({'uid': fight['uid']}, {'$push': {f"{user}_team": character_doc}, '$set': {'u1_ready': False, 'u2_ready': False}})
            self.user_db.find_one_and_update({'userid': self.user_id, 'collection.waifus.uid': character_doc['uid']}, {'$set': {'collection.waifus.$.available': False}})
            return True
        return "Unavailable"

    def fight_remove(self, index):
        fight = self.fight_get(user_id=self.user_id)
        user = "u1" if fight['u1'] == self.user_id else "u2"
        if len(self.user_document['collection']['waifus']) < index - 1:
            return IndexError

        if self.user_document['collection']['waifus'][index - 1] not in fight[f"{user}_offer"]:
            return False

        character_doc = self.get_local_character(index)
        self.fight_db.find_one_and_update({'uid': fight['uid']}, {'$pull': {f"{user}_team": character_doc}, '$set': {'u1_ready': False, 'u2_ready': False}})
        self.user_db.find_one_and_update({'userid': self.user_id, 'collection.waifus.uid': character_doc['uid']}, {'$set': {'collection.waifus.$.available': True}})
        return True

    def fight_ready(self):
        fight = self.fight_get(user_id=self.user_id)
        user = "u1" if fight['u1'] == self.user_id else "u2"
        if len(fight[f'{user}_team'] == 0):
            return "NoTeam"

        if fight[f'{user}_ready']:
            return False

        fight = self.fight_db.find_one_and_update({'uid': fight['uid']}, {'$set': {f'{user}_ready': True}}, return_document=pymongo.ReturnDocument.AFTER)
        if fight['u1_ready'] and fight['u2_ready']:
            return "Start"
        return True

    def fight_unready(self):
        fight = self.fight_get(user_id=self.user_id)
        user = "u1" if fight['u1'] == self.user_id else "u2"
        if fight[f'{user}_ready']:
            self.fight_db.find_one_and_update({'uid': fight['uid']}, {'$set': {f'{user}_ready': False}})
            return True
        return False

    def fight_start(self, uid):
        self.fight_db.find_one_and_update({'uid': uid}, {'$set': {'running': True}})

    def fight_switch(self, uid):
        fight_doc = self.fight_db.find_one({'uid': uid})
        turn = "u1" if fight_doc['turn'] == "u2" else "u1"
        if fight_doc[f'{turn}_team'][f"{fight_doc[f'{turn}_current']}"]['HP'] < 0:
            self.user_db.find_one_and_update({'userid': fight_doc[f'{turn}'], 'collection.waifus.uid': fight_doc[f'{turn}_team'][0]['uid']}, {'$set': {'collection.waifus.$.available': True}})
            fight_doc = self.fight_db.find_one_and_update({'uid': uid}, {'$inc': {f'{turn}_current': 1}})
            if len(fight_doc[f'{turn}_team']) <= fight_doc[f'{turn}_current']:
                return "Finished"
        self.fight_db.find_one_and_update({'uid': uid}, {'$set': {'turn': "u1" if turn == "u2" else "u2"}})
        return True

    def fight_execute_attack(self, uid, attack_id):
        fight_doc = self.fight_get(fight_uid=uid)

        attacker_waifu = fight_doc[f"{'u1' if fight_doc['turn'] == 'u1' else 'u2'}_team"][0]
        defender_waifu = fight_doc[f"{'u2' if fight_doc['turn'] == 'u1' else 'u1'}_team"][0]
        target = "u2" if fight_doc["turn"] == "u1" else "u1"

        attack = self.attack_db.find_one({'uid': attacker_waifu['attacks'][attack_id]})
        damage, critical = self.fight_calc_damage(attacker=attacker_waifu, defender=defender_waifu, attack=attack)

        if damage is None:
            return "Missed", None, None

        self.fight_db.find_one_and_update({'uid': uid}, {'$inc': {f'{target}_team.0.HP': -damage}})
        if damage < defender_waifu['HP']:
            return damage, critical[1], False
        else:
            return damage, critical[1], True

    def fight_calc_damage(self, attacker, defender, attack, adventure=False):
        attacker_element_data = self.element_db.find_one({'type': attacker['element']})
        defender_element_data = self.element_db.find_one({'type': defender['element']})

        if adventure:
            pass  # TODO Add Booster stuff if applicable

        if random.uniform(0, 1) > attack['acc']:
            return None, None

        critical = [2, True] if random.uniform(0, 1) <= attack['crit'] else [1, False]
        stab = 1.5 if attack['element'] == attacker['element'] else 0.5 if attack['element'] in attacker_element_data['weak_against'] else 1
        type_advantage = 2 if attack['element'] in defender_element_data['weak_against'] else 0.5 if attack['element'] in defender_element_data['strong_against'] else 1
        damage = int(round((((((((((2 * attacker['LVL'] / 5 + 2) * attacker['ATK'] * attack['atk']) / defender['DEF']) / 50) + 2) * stab) * type_advantage) * random.randint(217, 255)) / 255) * critical[0], 0))
        return damage, critical[1]

    # TODO Set all used Waifus to available again
    # def fight_complete(self):
    #     trade = self.trade_get(user_id=self.user_id)
    #     for character in trade['u1_offer']:
    #         self.remove_character(character['local_id'], user=trade['user_1'])
    #         character['local_id'] = len(self.user_db.find_one({'userid': trade['user_2']})['collection']['waifus']) + 1
    #         self.user_db.find_one_and_update({'userid': trade['user_2']}, {'$push': {'waifus': character}})
    #     for character in trade['u2_offer']:
    #         self.remove_character(character['local_id'], user=trade['user_2'])
    #         character['local_id'] = len(self.user_db.find_one({'userid': trade['user_1']})['collection']['waifus']) + 1
    #         self.user_db.find_one_and_update({'userid': trade['user_1']}, {'$push': {'waifus': character}})
    #     self.trade_db.delete_one({'uid': trade['uid']})

    def fight_get(self, fight_uid=None, user_id=None, ):
        if fight_uid is not None:
            fight = self.fight_db.find_one({'uid': fight_uid})
            return fight
        elif user_id is not None:
            fight = self.fight_db.find_one({'u1': user_id, 'u2': self.user_id})
            return fight
        else:
            fight = self.fight_db.find_one({'$or': [{'u1': self.user_id}, {'u2': self.user_id}]})
            return fight

    def fight_get_img(self, uid, names):
        doc = self.fight_db.find_one({'uid': uid})
        lists = []
        for user in range(2):
            waifu_list = []
            for i in range(len(doc[f'user_{user + 1}_offer'])):
                local_doc = doc[f'user_{user + 1}_offer'][i]
                waifu_doc = self.get_character(local_doc['id'])
                name = f"{local_doc['nickname']} ({waifu_doc['name']})" if local_doc['nickname'] is not None else waifu_doc['name']
                waifu_list.append([f"{local_doc['local_id']}", f"{local_doc['id']}", name, f"LVL {local_doc['LVL']}", f"HP {local_doc['HP']}", f"ATK {local_doc['ATK']}", f"DEF {local_doc['DEF']}"])
            lists.append(waifu_list)

        edited_list, content_length = RegularExtensions.sbs_string_extender(lists, " | ", "\t")
        edited_string = '\n'.join(edited_list)

        string = f'''{names[0] + "'s Team:" + " " * (content_length - len(names[0] + "'s Team:"))}   {names[1] + "'s Team:" + " " * (content_length - len(names[1] + "'s Team:"))}\n\n{edited_string}'''
        image = draw_fight(string)
        return image

    def fight_update_status(self, uid, message_data):
        self.fight_db.find_one_and_update({'uid': uid}, {'$set': {'last_status': message_data}})

    #########################
    #      INTERACTING      #
    #########################

    def interact(self, local_id):
        character = self.get_local_character(local_id)
        if character is None:
            return None, ["NotFound", None]

        character_doc = self.character_db.find_one({'id': character['id']})
        character['doc'] = character_doc
        if character['interaction_cooldown'] > RegularExtensions.epochnow():
            return None, ["Cooldown", character]
        elif (not character['married'] and character['Affection'] == 100) or (character['married'] and character['Affection'] == 150):
            return None, ["Max", character]
        amount = 0
        wait_time = 0
        if character['LVL'] == self.config_data['WaifuLaifu']['MaxLevel']:
            amount = random.randint(4, 5)
            wait_time = 10800
        elif character['rating'] == "R":
            amount = random.randint(1, 3)
            wait_time = 86400
        elif character['rating'] == "SR":
            amount = random.randint(2, 4)
            wait_time = 43200
        elif character['rating'] == "SSR":
            amount = random.randint(3, 5)
            wait_time = 21600
        if character['married'] and character['Affection'] + amount > 150:
            character = self.user_db.find_one_and_update({'userid': self.user_id, 'collection.waifus.local_id': local_id}, {'$set': {'collection.waifus.$.Affection': 150, 'collection.waifus.$.interaction_cooldown': RegularExtensions.epochnow() + wait_time}},return_document=pymongo.ReturnDocument.AFTER)
        elif not character['married'] and character['Affection'] + amount > 100:
            character = self.user_db.find_one_and_update({'userid': self.user_id, 'collection.waifus.local_id': local_id}, {'$set': {'collection.waifus.$.Affection': 100, 'collection.waifus.$.interaction_cooldown': RegularExtensions.epochnow() + wait_time}}, return_document=pymongo.ReturnDocument.AFTER)
        else:
            character = self.user_db.find_one_and_update({'userid': self.user_id, 'collection.waifus.local_id': local_id}, {'$inc': {'collection.waifus.$.Affection': amount, 'collection.waifus.$.interaction_cooldown': RegularExtensions.epochnow() + wait_time}}, return_document=pymongo.ReturnDocument.AFTER)
        character['doc'] = character_doc
        return character, amount

    def interact_all(self):
        if self.user_document['premium'] > 0:
            for character in self.user_document['collection']['waifus']:
                self.interact(character['id'])

    #########################
    #       ADVENTURES      #
    #########################

    def adventure_start(self, local_id):
        character_doc = self.get_local_character(local_id)
        if character_doc is None:
            return "NoCharacter"
        elif not character_doc['available']:
            return "Unavailable"
        adventure = {
            'uid': uuid.uuid4().hex,
            'user': self.user_id,
            'character': character_doc,
            'rewards': {
                'gems': 0,
                'exp': 0
            },
            'stats': {
                'start_time': RegularExtensions.epochnow(),
                'fights': 0,
                'events': 0
            },
            'next_event': RegularExtensions.epochnow() + 3600 + random.randint(-1800, 1800)
        }
        self.adventure_db.insert(adventure)
        self.user_db.find_one_and_update({'userid': self.user_id, 'collection.waifus.uid': character_doc['uid']}, {'$set': {'collection.waifus.$.available': False}})  # TODO Fix
        return adventure

    def adventure_stop(self, local_id=None, uid=None):
        if uid is not None:
            adventure_doc = self.adventure_db.find_one({'uid': uid})
        else:
            character_doc = self.get_local_character(local_id)
            if character_doc is None:
                return "NoCharacter", None, None
            adventure_doc = self.adventure_db.find_one({'character.uid': character_doc['uid']})
        if adventure_doc is None:
            return "NoDoc", None, None
        self.adventure_db.delete_one({'uid': adventure_doc['uid']})
        self.user_db.find_one_and_update({'userid': adventure_doc['user'], 'collection.waifus.uid': adventure_doc['character']['uid']}, {'$set': {'collection.waifus.$.available': True, 'collection.waifus.$.HP': adventure_doc['character']['HP']}, '$inc': {'collection.gems': adventure_doc['rewards']['gems']}})  # TODO Add EXP, set HP
        return True, adventure_doc['rewards']['gems'], adventure_doc['rewards']['exp']

    def adventure_get(self, local_id=None, uid=None):
        if uid is not None:
            return self.adventure_db.find_one({'uid': uid})
        elif local_id is not None:
            character_doc = self.get_local_character(local_id)
            return self.adventure_db.find_one({'character.uid': character_doc['uid']})
        return None

    def adventure_start_fight(self, uid):
        adventure_doc = self.adventure_db.find_one({'uid': uid})
        enemy_data = self.character_get_data()
        enemy_data['name'] = random.choice(list(self.character_db.find({})))['name']

        fight_log = f"Your $cname$ encountered a wild {enemy_data['name']}!\n"

        self.level_up(enemy_data, random.randint(adventure_doc['character']['LVL']-15 if adventure_doc['character']['LVL'] > 15 else 0, adventure_doc['character']['LVL']-1))

        turn = "u"
        while enemy_data['HP'] > 0 and adventure_doc['character']['HP'] > 0:
            attack = self.attack_db.find_one({'uid': random.choice(adventure_doc['character']['attacks']) if turn == "u" else random.choice(enemy_data['attacks'])})
            damage, critical = self.fight_calc_damage(attacker=adventure_doc['character'] if turn == "u" else enemy_data, defender=enemy_data if turn == "u" else adventure_doc['character'], attack=attack)
            if damage is None:
                fight_log += f"{'$cname$' if turn == 'u' else enemy_data['name']} used {attack['name']}. The attack missed!\n"
            else:
                if turn == "u":
                    enemy_data['HP'] -= damage
                elif turn == "e":
                    adventure_doc['character']['HP'] -= damage
                fight_log += f"{'$cname$' if turn == 'u' else enemy_data['name']} used {attack['name']}. It dealt {damage} Damage!\n"
            turn = "e" if turn == "u" else "u"
        if enemy_data['HP'] <= 0:
            fight_log += f"{enemy_data['name']} was defeated!"
            self.adventure_db.find_one_and_update({'uid': uid}, {'$set': {'character.HP': adventure_doc['character']['HP']}})  # TODO Add EXP / Gems.
            return True, fight_log
        elif adventure_doc['character']['HP'] <= 0:
            fight_log += f"$cname$ has been defeated!"
            self.adventure_db.find_one_and_update({'uid': uid}, {'$set': {'character.HP': 0}})  # TODO Add EXP / Gems.
            return False, None, fight_log

    def adventure_update(self, uid):
        return self.adventure_db.find_one_and_update({'uid': uid}, {'$set': {'next_event': RegularExtensions.epochnow() + 3600 + random.randint(-1800, 1800)}, '$inc': {'stats.fights': 1}}, return_document=pymongo.ReturnDocument.AFTER)['next_event']

    #########################
    #        CLAIMING       #
    #########################

    def get_claim_info(self, character_id):
        character = self.character_db.find_one({'id': character_id})
        return character

    def get_claim_character(self):
        character = list(self.character_db.aggregate([{'$sample': {'size': 1}}]))
        return character[0]
        # TODO Add Check if Character has a currently running Event Version

    def set_current_claim(self, message_id, character_name, character_id):
        spawn_list = self.server_db.find_one({'serverid': self.server_id})['current_spawns']
        removed_character = None
        if len(spawn_list) == 3:
            removed_character = spawn_list[2]
            spawn_list.pop(2)
        spawn_list.insert(0, {'m_id': message_id, 'c_name': character_name, 'c_id': character_id, 'claimed': False})
        pushed_characters = spawn_list[1:]
        self.server_db.find_one_and_update({'serverid': self.server_id}, {'$set': {'current_spawns': spawn_list}})
        return removed_character, pushed_characters

    def set_claimed(self, character_id):
        self.server_db.find_one_and_update({'serverid': self.server_id, 'current_spawns.c_id': character_id}, {'$set': {'current_spawns.$.claimed': True}})

    def check_claim_match(self, data):
        if len(self.server_document['current_spawns']) == 0:
            return IndexError, None, None

        highest_match = None
        highest_match_rate = 0
        highest_match_mismatch_counter = 0
        for i in range(len(self.server_document['current_spawns'])):
            if highest_match is not None and highest_match_mismatch_counter == 0:
                break
            mismatch_counter = 0
            for j in range(len(data)):
                if len(self.server_document['current_spawns'][i]['c_name']) <= j:
                    mismatch_counter += 1
                    continue
                if data[j].lower() != self.server_document['current_spawns'][i]['c_name'][j].lower():
                    mismatch_counter += 1
            if len(data) > len(self.server_document['current_spawns'][i]['c_name']):
                mismatch_counter += len(data) - len(self.server_document['current_spawns'][i]['c_name'])
            elif len(data) < len(self.server_document['current_spawns'][i]['c_name']):
                mismatch_counter += len(self.server_document['current_spawns'][i]['c_name']) - len(data)
            if 1 - mismatch_counter / len(self.server_document['current_spawns'][i]['c_name']) > highest_match_rate:
                highest_match = self.server_document['current_spawns'][i]
                highest_match_mismatch_counter = mismatch_counter

        for i in range(len(self.server_document['current_spawns'])):
            if self.user_document['premium'] == 0 and i > 0:
                return "NotPremium", None, True

            if highest_match != self.server_document['current_spawns'][i]:
                continue

            if self.server_document['current_spawns'][i]['claimed']:
                return "Claimed", highest_match, None

            elif highest_match_mismatch_counter == 0:
                return True, self.server_document['current_spawns'][i], None

            else:
                if i > 0:
                    return False, [highest_match, highest_match_mismatch_counter], True
                else:
                    return False, [highest_match, highest_match_mismatch_counter], False

    def add_claim_character(self, character):
        character = self.character_db.find_one({'id': character})

        doc = self.user_db.find_one_and_update({'userid': self.user_id, 'collection.wishlist.character_id': character['id']}, {'$pull': {'collection.wishlist': {'character_id': character['id']}}}, return_document=pymongo.ReturnDocument.AFTER)
        if doc is not None:
            self.character_db.find_one_and_update({'id': character['id']}, {'$inc': {'wishlist': -1}})

        character_data = self.character_get_data()

        self.user_db.find_one_and_update({'userid': self.user_id}, {'$push': {
            'collection.waifus': {
                'uid': uuid.uuid4().hex,
                'local_id': len(self.user_document['collection']['waifus']) + 1,
                'id': character['id'],
                'nickname': None,
                'married': False,
                'rating': character_data['rating'].name,
                'element': character_data['element'],
                'stat_set_name': character_data['base_stat_set']['name'],
                'base_stats': character_data['base_stats'],
                'max_stats': character_data['max_stats'],
                'attacks': character_data['attacks'],
                'LVL': character_data['LVL'],
                'ATK': character_data['ATK'],
                'DEF': character_data['DEF'],
                'HP': character_data['HP'],
                'Affection': 0,
                'Winstreak': 0,
                'interaction_cooldown': 0,
                'available': True,
                'grand_eligible': True if character_data['rating'].name in ["R", "SR"] else False
            }
        }})
        # TODO Add Stuff to differentiate Normal from Special Characters

        set_result = self.character_set_check(character)
        if len(set_result) != 0:
            return character, set_result
        return character, None

    def character_get_data(self):
        data = {}

        rand = random.uniform(0, 1)
        if rand < 0.81:
            data['rating'] = Rating.R
        elif rand < 0.93:
            data['rating'] = Rating.SR
        else:
            data['rating'] = Rating.SSR

        data['LVL'] = 1
        data['base_stat_set'] = list(self.base_stat_db.aggregate([{'$sample': {'size': 1}}]))[0]
        data['base_stats'] = self.get_base_stats(data['rating'].value, data['base_stat_set'])
        data['HP'] = int(round((((2 * data['base_stats'][0]) * data['LVL']) / 100) + data['LVL'] + 10, 0))
        data['ATK'] = int(round((((2 * data['base_stats'][1]) * data['LVL']) / 100) + 5, 0))
        data['DEF'] = int(round((((2 * data['base_stats'][2]) * data['LVL']) / 100) + 5, 0))
        data['max_stats'] = [data['base_stats'][0] * 2 + 5, data['base_stats'][1] * 2 + 5, data['base_stats'][2] * 2 + 110]

        data['element'] = list(self.element_db.aggregate([{'$sample': {'size': 1}}]))[0]['type']

        normal_attack = list(self.attack_db.aggregate([{'$match': {'element': "neutral", 'category': "normal"}}, {'$sample': {'size': 1}}]))
        char_attacks = list(self.attack_db.aggregate([{'$match': {'category': "normal", 'uid': {'$ne': normal_attack[0]['uid']}}}, {'$sample': {'size': 3}}])) + normal_attack
        random.shuffle(char_attacks)

        char_attack_ids = []
        for attack in char_attacks:
            char_attack_ids.append(attack['uid'])
        data['attacks'] = char_attack_ids

        data['next_level'] = ((4 * data['LVL'] ** 3) / 5) - (((4 * (data['LVL'] - 1)) ** 3) / 5) if data['rating'].name == "R" else data['LVL'] ** 3 - (data['LVL'] - 1) ** 3 if data['rating'].name == "SR" else (5 * data['LVL'] ** 3) / 4 - (((5 * (data['LVL'] - 1)) ** 3) / 5)

        return data

    def character_set_check(self, character):
        completion_list = []
        waifu_ids = self.get_id_list()
        if character['id'] not in waifu_ids:
            sets = list(self.set_db.find({'characters': {'$in': [character['id']]}}))
            for card_set in sets:
                counter = 1
                for set_character in card_set['characters']:
                    if set_character in waifu_ids:
                        counter += 1
                if counter == len(card_set['characters']):
                    completion_list.append([card_set['set_name'], 1])
                elif counter == round(len(card_set['characters']) / 2, 0):
                    completion_list.append([card_set['set_name'], 0.5])
        return completion_list

    def get_base_stats(self, rating, stat_set):
        attack = random.randint(stat_set['atk'][0], stat_set['atk'][1])
        defense = random.randint(stat_set['def'][0], stat_set['def'][1])
        health = random.randint(stat_set['hp'][0], stat_set['hp'][1])

        for i in range(rating):
            attack += random.randint(stat_set['rarity_increase']['atk'][0], stat_set['rarity_increase']['atk'][1])
            defense += random.randint(stat_set['rarity_increase']['def'][0], stat_set['rarity_increase']['def'][1])
            health += random.randint(stat_set['rarity_increase']['hp'][0], stat_set['rarity_increase']['hp'][1])

        return attack, defense, health

    #########################
    #          SHOP         #
    #########################

    def item_get(self, item_id):
        return self.item_db.find_one({'id': item_id})

    def shop_get(self):
        return self.server_db.find_one({'serverid': self.server_id})['shop']

    def shop_buy(self, item_id):
        item = self.item_get(item_id)
        if item['price'] > self.user_document['collection']['gems']:
            return "NotEnoughGems"

        i = {
            'uid': uuid.uuid4().hex,
            'id': item['id'],
            'type': item['type']
        }
        if item['type'] == "consumable":
            i['uses'] = item['uses']
        elif item['type'] == "booster":
            i['expiration'] = item['expiration']

        self.user_db.find_one_and_update({'userid': self.user_id}, {'$push': {'collection.items': i}, '$inc': {'collection.gems': -item['price']}})
        return True

    def shops_refresh(self, server_id=None):
        if server_id is None:
            servers = self.server_db.find({})
        else:
            servers = self.server_db.find({'serverid': server_id})
        for server in servers:
            items = list(self.item_db.aggregate([{'$sample': {'size': 5}}]))
            for i in range(len(items)):
                items[i] = items[i]['id']
            self.server_db.find_one_and_update({'serverid': server['serverid']}, {'shop': items})
        return True

    ##############################
    #         ITEM USAGE         #
    ##############################

    def boosters_get(self):
        inactive = []
        active = []
        for booster in self.user_document['collection']['boosters']:
            if booster['active']:
                active.append(booster)
            else:
                inactive.append(booster)
        return {'inactive': inactive, 'active': active}

    def booster_activate(self, uid):
        for booster in self.user_document['collection']['boosters']:
            if booster['uid'] != uid:
                continue

            if booster['active']:
                return "AlreadyActive"
            else:
                db_booster = self.item_get(booster['id'])
                self.user_db.find_one_and_update({'userid': self.user_id, 'collection.boosters.uid': uid}, {'$set': {'collection.boosters.$': {'active': True, 'expiration': RegularExtensions.epochnow() + db_booster['expiration']}}})
        return "NotFound"

    def booster_remove(self, uid, user=None):
        if user is not None:
            self.load_user(user)
        self.user_db.find_one_and_update({'userid': self.user_id, 'collection.boosters.uid': uid}, {'$pull': {'collection.boosters.$': None}})

    ##############################
    #       SERVER SETTINGS      #
    ##############################

    def set_prefix(self, prefix):
        self.server_db.find_one_and_update({'serverid': self.server_id}, {'$set': {'settings.command_prefix': prefix}})
        return None

    def set_spawnchannel(self, channel):
        self.server_db.find_one_and_update({'serverid': self.server_id}, {'$set': {'settings.spawn_channel': channel.name}})
        return None

    ##############################
    #    WISHLIST / FAVORITES    #
    ##############################

    def favorites_add(self, data):
        if RegularExtensions.intTryParse(data):
            character_data = self.character_db.find_one({'$or': [{'id': int(data)}, {'name': bson.regex.Regex(f"^({data}|{RegularExtensions.reverse_words(data)})|({data}|{RegularExtensions.reverse_words(data)})$", flags=re.IGNORECASE)}]})
            if character_data is None:
                local_character = self.get_local_character(int(data))
                if local_character is not None:
                    character_data = self.character_db.find_one({'id': local_character['id']})
        else:
            character_data = self.character_db.find_one({'$or': [{'id': int(data)}, {'name': bson.regex.Regex(f"^({data}|{RegularExtensions.reverse_words(data)})|({data}|{RegularExtensions.reverse_words(data)})$", flags=re.IGNORECASE)}, {'collection.waifus.nickname': data}]})
        if character_data is None:
            return "NotFound", None

        if self.user_db.find_one({'userid': self.user_id, 'collection.favorites.id': character_data['id']}) is not None:
            return "InList", character_data['name']
        self.user_db.find_one_and_update({'userid': self.user_id}, {'$push': {'collection.favorites': {'id': character_data['id'], 'name': character_data['name'], 'position': len(self.user_document['collection']['favorites']) + 1}}})
        self.character_db.find_one_and_update({'id': character_data['id']}, {'$inc': {'favorites': 1}})
        return True, character_data['name']

    def favorites_remove(self, data):
        local_character = None if not RegularExtensions.intTryParse(data) else self.get_local_character(int(data))
        for character in self.user_document['collection']['favorites']:
            if (RegularExtensions.intTryParse(data) and int(data) == character['id']) or (local_character is not None and local_character['id'] == character['id']) or (re.search(rf"^({data}|{RegularExtensions.reverse_words(data)})|({data}|{RegularExtensions.reverse_words(data)})$", character['name'], flags=re.IGNORECASE)):
                self.user_db.find_one_and_update({'userid': self.user_id}, {'$pull': {'collection.favorites': character}})
                self.user_db.find_one_and_update({'userid': self.user_id, 'collection.favorites.position': {'$gt': character['position']}}, {'$inc': {'collection.favorites.$[].position': -1}})
                self.character_db.find_one_and_update({'id': character['id']}, {'$inc': {'favorites': -1}})
                return True, character['name']
        return "NotFound", None

    def favorites_get(self):
        def get_key(char):
            return char['position']

        list = sorted(self.user_document['collection']['favorites'], key=get_key)

    def favorites_set(self, data):
        position = RegularExtensions.intTryParse(data[-1:])
        if position is None:
            return "NoPosition"

        data = data[:-1].rstrip()
        local_character = None if not RegularExtensions.intTryParse(data) else self.get_local_character(int(data))
        for character in self.user_document['collection']['favorites']:
            if (RegularExtensions.intTryParse(data) and int(data) == character['id']) or (local_character is not None and local_character['id'] == character['id']) or (re.search(rf"^({data}|{RegularExtensions.reverse_words(data)})|({data}|{RegularExtensions.reverse_words(data)})$", character['name'], flags=re.IGNORECASE)):
                if character['position'] == position:
                    return "SamePosition", character['name']
                if character['position'] > position:
                    self.user_db.update_many({'userid': self.user_id, 'collection.favorites.position': {'$gte': position, '$lt': character['position']}}, {'$inc': {'collection.favorites.$[].position': 1}})
                elif character['position'] < position:
                    self.user_db.update_many({'userid': self.user_id, 'collection.favorites.position': {'$lt': position, '$gt': character['position']}}, {'$inc': {'collection.favorites.$[].position': -1}})
                self.user_db.find_one_and_update({'userid': self.user_id, 'collection.favorites.id': character['id']}, {'$set': {'collection.favorites.$.position': position}})
                return True, character['name']

    def wishlist_add(self, data):
        if RegularExtensions.intTryParse(data):
            character_data = self.character_db.find_one({'$or': [{'id': int(data)}, {'name': bson.regex.Regex(f"^({data}|{RegularExtensions.reverse_words(data)})|({data}|{RegularExtensions.reverse_words(data)})$", flags=re.IGNORECASE)}]})
        else:
            character_data = self.character_db.find_one({'name': bson.regex.Regex(f"^({data}|{RegularExtensions.reverse_words(data)})|({data}|{RegularExtensions.reverse_words(data)})$", flags=re.IGNORECASE)})
        if character_data is None:
            return "NotFound", None

        if self.user_db.find_one({'userid': self.user_id, 'collection.wishlist.id': character_data['id']}) is not None:
            return "InList", character_data['name']
        if self.user_db.find_one({'userid': self.user_id, 'collection.waifus.id': character_data['id']}) is not None:
            return "Owned", character_data['name']
        self.user_db.find_one_and_update({'userid': self.user_id}, {'$push': {'collection.wishlist': {'id': character_data['id'], 'name': character_data['name'], 'position': len(self.user_document['collection']['wishlist']) + 1}}})
        self.character_db.find_one_and_update({'id': character_data['id']}, {'$inc': {'wishlist': 1}})
        return True, character_data['name']

    def wishlist_remove(self, data):
        for character in self.user_document['collection']['wishlist']:
            if (RegularExtensions.intTryParse(data) and int(data) == character['id']) or (re.search(rf"^({data}|{RegularExtensions.reverse_words(data)})|({data}|{RegularExtensions.reverse_words(data)})$", character['name'], flags=re.IGNORECASE)):
                self.user_db.find_one_and_update({'userid': self.user_id}, {'$pull': {'collection.wishlist': character}})
                self.user_db.find_one_and_update({'userid': self.user_id, 'collection.wishlist.position': {'$gt': character['position']}}, {'$inc': {'collection.wishlist.$[].position': -1}})
                self.character_db.find_one_and_update({'id': character['id']}, {'$inc': {'wishlist': -1}})
                return True, character['name']
        return "NotFound", None

    def wishlist_get(self):
        def get_key(char):
            return char['position']

        return sorted(self.user_document['collection']['wishlist'], key=get_key)

    def wishlist_set(self, data):
        position = RegularExtensions.intTryParse(data[-1:])
        if position is None:
            return "NoPosition", None

        data = data[:-1].rstrip()
        for character in self.user_document['collection']['wishlist']:
            if (RegularExtensions.intTryParse(data) and int(data) == character['id']) or (re.search(rf"^({data}|{RegularExtensions.reverse_words(data)})|({data}|{RegularExtensions.reverse_words(data)})$", character['name'], flags=re.IGNORECASE)):
                if character['position'] == position:
                    return "SamePosition", character['name']
                if character['position'] > position:
                    self.user_db.update_many({'userid': self.user_id, 'collection.wishlist.position': {'$gte': position, '$lt': character['position']}}, {'$inc': {'collection.wishlist.$[].position': 1}})
                elif character['position'] < position:
                    self.user_db.update_many({'userid': self.user_id, 'collection.wishlist.position': {'$lt': position, '$gt': character['position']}}, {'$inc': {'collection.wishlist.$[].position': -1}})
                self.user_db.find_one_and_update({'userid': self.user_id, 'collection.wishlist.id': character['id']}, {'$set': {'collection.wishlist.$.position': position}})
                return True, character['name']


class Rating(Enum):
    R = 0
    SR = 1
    SSR = 2
