import datetime
import io
from PIL import Image, ImageDraw, ImageFont


class RegularExtensions:

    @staticmethod
    def epochnow():
        return int(datetime.datetime.now().timestamp())

    @staticmethod
    def string_extender(data, connector=None, extender=" ", before=False):
        for field in range(len(data[0])):  # All Arrays should have the same size
            longest = 0
            for array in range(len(data)):  # Determining longest Field
                data[array][field] = str(data[array][field])
                if len(data[array][field]) > longest:
                    longest = len(data[array][field])

            for array in range(len(data)):  # Adding Spaces
                if len(data[array][field]) < longest:
                    if before:
                        data[array][field] = extender * (longest - len(data[array][field])) + data[array][field]
                    else:
                        data[array][field] = data[array][field] + (extender * (longest - len(data[array][field])))

        if connector is not None:
            for array in range(len(data)):  # Join Arrays with Connector
                data[array] = connector.join(data[array])

        return data

    @staticmethod
    def sbs_string_extender(data, connector=None, separator=None, extender=" "):
        fields = 0
        for side_array in data:
            if len(side_array) != 0:
                if len(side_array[0]) > fields:
                    fields = len(side_array[0])
        for field in range(fields):
            longest = 0
            for side_array in range(len(data)):
                for content_array in range(len(data[side_array])):
                    data[side_array][content_array][field] = str(data[side_array][content_array][field])
                    if len(data[side_array][content_array][field]) > longest:
                        longest = len(data[side_array][content_array][field])

            for side_array in range(len(data)):
                for content_array in range(len(data[side_array])):
                    if len(data[side_array][content_array][field]) < longest:
                        data[side_array][content_array][field] += extender * (longest - len(data[side_array][content_array][field]))

        if connector is not None:
            for side_array in range(len(data)):
                for content_array in range(len(data[side_array])):
                    data[side_array][content_array] = connector.join(data[side_array][content_array])

        edited_lines = []
        longest_content = [0, 0]
        for side_array in range(len(data)):
            if len(data[side_array]) > longest_content[1]:
                longest_content[0] = side_array
                longest_content[1] = len(data[side_array])

        content_length = len(data[longest_content[0]][0])
        for character_field in range(len(data[longest_content[0]])):
            string = ""
            for side_array in range(len(data)):
                if len(data[side_array]) > character_field:
                    string += data[side_array][character_field]
                else:
                    string += extender * len(data[longest_content[0]][0])
                if separator is not None and side_array != (len(data) - 1):
                    string += separator
            edited_lines.append(string)

        return edited_lines, content_length

    @staticmethod
    def intTryParse(value):
        try:
            return int(value)
        except (ValueError, TypeError):
            return None

    @staticmethod
    def reverse_words(input_string):
        words = input_string.split(" ")
        words = words[-1::-1]
        output = ' '.join(words)
        return output

    @staticmethod
    def get_time_difference(start=None, end=None):
        if start is None:
            start = RegularExtensions.epochnow()
        if end is None:
            end = RegularExtensions.epochnow()

        remaining_time = end - start
        remaining_hours = int(remaining_time / 3600)
        remaining_minutes = int((remaining_time - remaining_hours * 3600) / 60)
        remaining_seconds = int(remaining_time - remaining_hours * 3600 - remaining_minutes * 60)
        if len(str(remaining_hours)) != 2:
            remaining_hours = "0" + str(remaining_hours)
        if len(str(remaining_minutes)) != 2:
            remaining_minutes = "0" + str(remaining_minutes)
        if len(str(remaining_seconds)) != 2:
            remaining_seconds = "0" + str(remaining_seconds)
        return f"{remaining_hours}:{remaining_minutes}:{remaining_seconds}"


def draw_trade(data):
    img = Image.new("RGB", (960, 540), "white")
    font = ImageFont.truetype("./fonts/Inconsolata-Regular.ttf", 12)
    font_width, font_height = font.getsize_multiline(data)
    draw = ImageDraw.Draw(img)
    draw.multiline_text(((img.width - font_width) / 2, (img.height - font_height) / 2), data, fill="black", font=font)

    b = io.BytesIO()
    img.save(b, "JPEG")
    return b.getvalue()


def draw_fight(data):
    img = Image.new("RGB", (960, 540), "white")
    font = ImageFont.truetype("./fonts/Inconsolata-Regular.ttf", 12)
    font_width, font_height = font.getsize_multiline(data)
    draw = ImageDraw.Draw(img)
    draw.multiline_text(((img.width - font_width) / 2, (img.height - font_height) / 2), data, fill="black", font=font)

    b = io.BytesIO()
    img.save(b, "JPEG")
    return b.getvalue()
