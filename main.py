import math
import configparser
import discord
import time
import asyncio
import os
import random
import logging
import subprocess
import re
import sys
from subprocess import call
from discord.ext import commands
from logging.handlers import TimedRotatingFileHandler
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from dateutil import parser
from dateutil.tz import tzutc
from WaifuLaifu import WaifuLaifu
from secret import *
from extensions import RegularExtensions


def setup_logger(name, log_file):
    if not os.path.exists("./logs"):
        os.makedirs("./logs")
    handler = TimedRotatingFileHandler(os.path.join("logs", f"{log_file}.log"), when="midnight")
    handler.setFormatter(logging.Formatter(fmt='[%(asctime)s]: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p'))

    logger = logging.getLogger(name)
    logger.setLevel(logging.WARNING)
    logger.addHandler(handler)

    return logger


def activate_logging():
    global gen_logger
    gen_logger = setup_logger('gen_logger', 'general')


def gen_log(message):
    print(f"{get_time()} {message}")
    gen_logger.warning(message)


def db_log(message, processing_time=None):
    if processing_time is None:
        processing_time = "-"
    print(f"{get_time()} [{processing_time}] {message}")
    gen_logger.warning(message)


def get_file_list(pathin):
    return [os.path.join(path, file)
            for (path, dirs, files) in os.walk(pathin)
            for file in files]


def get_time():
    now = datetime.datetime.now()
    return f"[{now.strftime('%Y-%m-%d %H:%M:%S')}]"


gen_logger = None
activate_logging()
config_data = configparser.ConfigParser()
config_data.read(Path("./data/data.ini"))
message_data = configparser.ConfigParser()
message_data.read(Path("./data/messages.ini"))
Client = discord.AutoShardedClient
client = commands.Bot(command_prefix="")

paused = True
client.remove_command('help')


async def get_scheduled_events():
    db_connection = WaifuLaifu()
    scheduled_fights = db_connection.fight_db.find({})
    counter = 0
    for fight_doc in scheduled_fights:
        db_connection.fight_delete(fight_doc['uid'])
        if fight_doc['accepted']:
            users = [client.get_user(fight_doc['u1']), client.get_user(fight_doc['u2'])]
            for user in users:
                await send_message(user, await get_message_data("Fighting", "CancelledDM", users=[user], doc=fight_doc))
            counter += 1
    gen_log(f"Cancelled {counter} Fights due to a crash / restart!")

    counter_scheduled = 0
    counter_executed = 0
    scheduled_adventures = db_connection.adventure_db.find({})
    for adventure_doc in scheduled_adventures:
        if adventure_doc['next_event'] < RegularExtensions.epochnow():
            await adventure_event(adventure_doc['uid'])
            counter_executed += 1
        else:
            scheduler.add_job(adventure_event, 'date', [adventure_doc['uid']], run_date=datetime.datetime.fromtimestamp(adventure_doc['next_event']), name=f"{adventure_doc['uid']} - Adventure Next Event", misfire_grace_time=300)
            counter_scheduled += 1
    gen_log(f"Scheduled {counter_scheduled} Adventures!")
    gen_log(f"Executed {counter_executed} Adventures!")


@client.event
async def on_ready():
    global paused
    print(f"{get_time()} Starting up Waifu Bot Thingy v{config_data['General']['Version']}!")
    print(f"{get_time()} I am running as {client.user.name}")
    print(f"{get_time()} With the ID: {client.user.id}")
    await client.change_presence(activity=discord.Game(name=config_data['General']['PlayingStatus']))
    print(f"{get_time()} Playing status set to {config_data['General']['PlayingStatus']}!")
    if not scheduler.running:
        scheduler.start()

    await get_scheduled_events()

    paused = False


##################################


async def delete_message(message, wait_time=0):
    await asyncio.sleep(wait_time)
    try:
        await message.delete()
    except discord.NotFound:
        pass


async def delete_command(message):
    if type(message.channel) is not discord.DMChannel:
        try:
            await message.delete()
        except discord.NotFound:
            pass


async def get_message_data(section, field, doc=None, users=None, values=None):
    other = None if users is None or doc is None else doc['u1'] if doc['u1'] == users[0].id else doc['u2']
    db_connection = WaifuLaifu(server=users[0].guild if users is not None else None)
    prefix = db_connection.server_document['settings']['command_prefix'] if db_connection.server_document is not None else db_connection.config_data['WaifuLaifu']['DefaultPrefix']

    message = message_data[section][field]
    message.replace("$doc1_display", client.get_user(doc['u1']).display_name if doc is not None else "ERR")\
           .replace("$doc2_display", client.get_user(doc['u2']).display_name if doc is not None else "ERR")\
           .replace("$doc1", client.get_user(doc['u1']).mention if doc is not None else "ERR")\
           .replace("$doc2", client.get_user(doc['u2']).mention if doc is not None else "ERR")\
           .replace("$u1_display", users[0].display_name if users is not None else client.get_user(doc['u1']).display_name if doc is not None else "ERR")\
           .replace("$u2_display", users[1].display_name if len(users) > 1 else client.get_user(doc['u2']).display_name if doc is not None else "ERR")\
           .replace("$u1", users[0].mention if users is not None else client.get_user(doc['u1']).mention if doc is not None else "ERR")\
           .replace("$u2", users[1].mention if len(users) > 1 else client.get_user(doc['u2']).mention if doc is not None else "ERR")\
           .replace("$prefix", prefix)\
           .replace("$other", client.get_user(other).mention if other is not None else "ERR")

    if type(values) not in (list, type(None)):
        message.replace("$value", values)
    elif type(values) == list:
        for i in range(message.count("$value")):
            message.replace(f"$value{i+1}", values[i])

    return message


async def send_message(channel, message=None, embed: discord.Embed = None, file=None):
    try:
        if file is not None:
            return await channel.send(message, embed=embed, file=discord.File(fp=file, filename="Cool File.jpeg"))
        else:
            return await channel.send(message, embed=embed)
    except discord.Forbidden:
        return None


async def get_message(channel, message_id):
    try:
        return await channel.get_message(message_id)
    except discord.NotFound:
        return None


async def add_emoji(message: discord.Message, emojis):
    for emoji in emojis:
        if type(emoji) == int:
            emoji = client.get_emoji(emoji)
        try:
            await message.add_reaction(emoji)
        except discord.Forbidden:
            return "Forbidden"
        except discord.HTTPException:
            print("Emoji not found")


def permission_check(user_id):
    if user_id in admin_list:
        return True
    else:
        return False


def get_color(data=None):
    if data is not None and type(data.channel) is discord.TextChannel:
        if data.guild is not None:
            return data.guild.me.color
    else:
        return discord.Color.from_rgb(255, 255, 255)


##################################
#            TRADING             #
##################################

@client.group(aliases=['t'])
async def trade(ctx):  # TODO If already trading and tagging another user, ask if you want to abandon the old trade
    """
    Main Trade Command. Prefix (including 't') for all other commands.
    :param ctx: Invocation Message Data
    :return: None
    """
    await delete_command(ctx.message)
    if ctx.invoked_subcommand is not None:
        return

    if len(ctx.message.mentions) == 0:
        await send_message(ctx.channel, "Trading Help")
        return

    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    if ctx.message.mentions[0] == ctx.author:
        msg = await send_message(ctx.channel, await get_message_data("Trade", "SelfTrade", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    if not db_connection.profile_check(ctx.message.mentions[0]):
        msg = await send_message(ctx.channel, await get_message_data("General", "NoProfile", users=[ctx.author, ctx.message.mentions[0]]))
        await delete_message(msg, 10)
        return

    trade_doc = db_connection.trade_get()
    if trade_doc is not None:
        if trade_doc['accepted']:
            await send_message(ctx.channel, await get_message_data("Trade", "AlreadyTrading", users=[ctx.author], doc=trade_doc))
        elif trade_doc['u2'] == ctx.author.id:
            db_connection.trade_accept(trade_doc['uid'])
            await send_message(ctx.channel, await get_message_data("Trade", "Accepted", users=[ctx.author], doc=trade_doc))
            await send_message(ctx.channel, "Show what they should do next")
            scheduler.add_job(trade_timeout, 'date', [trade_doc['uid'], "runtime"], run_date=datetime.datetime.fromtimestamp(RegularExtensions.epochnow() + 600), name=f"{trade_doc['uid']} - Trade 10 Minute Timeout")
        else:
            await send_message(ctx.channel, await get_message_data("Trade", "AlreadyTrading", users=[ctx.author], doc=trade_doc))
        return

    uid = db_connection.trade_start(ctx.message.mentions[0])
    await send_message(ctx.channel, await get_message_data("Trade", "Invite", users=[ctx.author, ctx.message.mentions[0]]))
    scheduler.add_job(trade_timeout, 'date', [uid, "accept"], run_date=datetime.datetime.fromtimestamp(RegularExtensions.epochnow() + 180), name=f"{uid} - Trade Accept 3 Minute Timeout")


@trade.command()
async def abandon(ctx):
    """
    Trade Command to abandon a Trade. Automatically picks the Trade associated to the User if applicable.
    :param ctx: Invocation Message Data including the User
    """
    await delete_command(ctx.message)
    db_connection = WaifuLaifu(user=ctx.author)
    trade_doc = db_connection.trade_get()
    if trade_doc is not None:
        db_connection.trade_delete(trade_doc['uid'])
        await send_message(ctx.channel, await get_message_data("Trade", "Cancelled", users=[ctx.author], doc=trade_doc))
    else:
        msg = await send_message(ctx.channel, await get_message_data("Trade", "NotTrading", users=[ctx.author]))
        await delete_message(msg, 10)


@trade.command()
async def accept(ctx, user: discord.Member = None):
    """
    Trade Command to accept a Trade. Automatically picks the Trade associated to the User if applicable.
    :param ctx: Invocation Message Data
    :param user: discord.Member that you want to accept the trade from.
    """
    await delete_command(ctx.message)
    if user is None:
        msg = await send_message(ctx.channel, await get_message_data("Trade", "NoAcceptTarget", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    db_connection = WaifuLaifu(user=ctx.author)
    trade_doc = db_connection.trade_get(user_id=user.id)
    if trade_doc is not None:
        db_connection.trade_accept(uid=trade_doc['uid'])
        await send_message(ctx.channel, await get_message_data("Trade", "Accepted", users=[ctx.author], doc=trade_doc))
        await send_message(ctx.channel, "Show what they should do next")
        scheduler.add_job(trade_timeout, 'date', [trade_doc['uid'], "runtime"], run_date=datetime.datetime.fromtimestamp(RegularExtensions.epochnow() + 600), name=f"{trade_doc['uid']} - Trade 10 Minute Timeout")
    else:
        msg = await send_message(ctx.channel, await get_message_data("Trade", "NotTradingWith", users=[ctx.author, ctx.message.mentions[0]]))
        await delete_message(msg, 10)


@trade.command()
async def add(ctx, character_index=None):
    """
    Trade Command to add a Character to your Offer.
    :param ctx: Invocation Message Data
    :param character_index: Local Index of the Character that you want to add
    :return: None
    """
    await delete_command(ctx.message)
    db_connection = WaifuLaifu(user=ctx.author)
    trade_doc = db_connection.trade_get()
    if trade_doc is None:
        msg = await send_message(ctx.channel, await get_message_data("Trade", "NotTrading", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    u1 = client.get_user(trade_doc['u1'])
    u2 = client.get_user(trade_doc['u2'])
    if RegularExtensions.intTryParse(character_index) is None:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "NoLocalIDAdd", users=[ctx.author], doc=trade_doc))
        await delete_message(msg, 10)
        return

    result = db_connection.trade_add(int(character_index))
    if result is True:
        image = db_connection.trade_get_img(trade_doc['uid'], [u1.display_name, u2.display_name])
        msg = await send_message(ctx.channel, message=await get_message_data("Trade", "TradeAdd", users=[ctx.author], doc=trade_doc), file=image)
        if trade_doc['last_status'] is not None:
            try:
                await delete_message(await client.get_channel(trade_doc['last_status'][0]).get_message(trade_doc['last_status'][1]))
            except discord.NotFound:
                pass
        db_connection.trade_update_status(trade_doc['uid'], [msg.channel.id, msg.id])
        return

    elif result is IndexError:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "SlotEmptyDoc", users=[ctx.author], doc=trade_doc))
    elif result is False:
        msg = await send_message(ctx.channel, await get_message_data("Trade", "AlreadyAdded", users=[ctx.author], doc=trade_doc))
    elif result is 10:
        msg = await send_message(ctx.channel, await get_message_data("Trade", "MaxTen", users=[ctx.author], doc=trade_doc))
    await delete_message(msg, 10)


@trade.command()
async def remove(ctx, character_index=None):
    """
    Trade Command to remove a Character from your Offer.
    :param ctx: Invocation Message Data.
    :param character_index: Local Index of the Character that you want to remove.
    :return: None
    """
    await delete_command(ctx.message)
    db_connection = WaifuLaifu(user=ctx.author)
    trade_doc = db_connection.trade_get()
    if trade_doc is None:
        msg = await send_message(ctx.channel, await get_message_data("Trade", "NotTrading", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    u1 = client.get_user(trade_doc['u1'])
    u2 = client.get_user(trade_doc['u2'])
    if RegularExtensions.intTryParse(character_index) is None:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "NoLocalIDRemove", users=[ctx.author], doc=trade_doc))
        await delete_message(msg, 10)
        return None

    result = db_connection.trade_remove(int(character_index))
    if result is True:
        image = db_connection.trade_get_img(trade_doc['uid'], [u1.display_name, u2.display_name])
        msg = await send_message(ctx.channel, message=await get_message_data("Trade", "TradeRemove", users=[ctx.author], doc=trade_doc), file=image)
        if trade_doc['last_status'] is not None:
            try:
                await delete_message(await client.get_channel(trade_doc['last_status'][0]).get_message(trade_doc['last_status'][1]), 0)
            except discord.NotFound:
                pass
        db_connection.trade_update_status(trade_doc['uid'], [msg.channel.id, msg.id])
        return

    elif result is IndexError:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "SlotEmptyDoc", users=[ctx.author], doc=trade_doc))
    elif result is False:
        msg = await send_message(ctx.channel, await get_message_data("Trade", "NotAdded", users=[ctx.author], doc=trade_doc))
    await delete_message(msg, 10)


@trade.command()
async def ready(ctx):
    """
    Trade Command to set yourself to ready.
    :param ctx: Invocation Message Data
    """
    await delete_command(ctx.message)
    db_connection = WaifuLaifu(user=ctx.author)
    trade_doc = db_connection.trade_get()
    if trade_doc is None:
        msg = await send_message(ctx.channel, await get_message_data("Trade", "NotTrading", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    result = db_connection.trade_ready()
    if result is True:
        await send_message(ctx.channel, await get_message_data("Shared", "NowReady", users=[ctx.author], doc=trade_doc))
    elif result is False:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "AlreadyReady", users=[ctx.author], doc=trade_doc))
        await delete_message(msg, 10)
    elif result is "Complete":
        await send_message(ctx.channel, await get_message_data("Shared", "NowReady", users=[ctx.author], doc=trade_doc))
        await send_message(ctx.channel, await get_message_data("Trade", "idkyet", users=[ctx.author], doc=trade_doc))
        timer = 0
        while True:
            await asyncio.sleep(1)
            timer += 1
            trade_doc = db_connection.trade_get(trade_uid=trade_doc['uid'])
            if not (trade_doc['u1_ready'] and trade_doc['u2_ready']):
                await send_message(ctx.channel, await get_message_data("Trade", "CompletionStopped", users=[ctx.author], doc=trade_doc))
                return
            if timer == 10:
                break
        trade_doc = db_connection.trade_get(trade_uid=trade_doc['uid'])
        if trade_doc['u1_ready'] and trade_doc['u2_ready']:
            db_connection.trade_complete()
            await send_message(ctx.channel, await get_message_data("Trade", "Completed", users=[ctx.author], doc=trade_doc))


@trade.command()
async def unready(ctx):
    """
    Trade Command to set yourself to not ready.
    :param ctx: Invocation Message Data
    """
    await delete_command(ctx.message)
    db_connection = WaifuLaifu(user=ctx.author)
    trade_doc = db_connection.trade_get()
    if trade_doc is None:
        msg = await send_message(ctx.channel, await get_message_data("Trade", "NotTrading", users=[ctx.author]))
        await delete_message(msg, 10)

    result = db_connection.trade_unready()
    if result:
        await send_message(ctx.channel, await get_message_data("Shared", "NowUnready", users=[ctx.author], doc=trade_doc))
    else:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "AlreadyUnready", users=[ctx.author], doc=trade_doc))
        await delete_message(msg, 10)


async def trade_timeout(uid, mode):
    db_connection = WaifuLaifu()
    trade_doc = db_connection.trade_get(trade_uid=uid)
    if trade_doc is None:
        return

    if not trade_doc['accepted'] and mode == "accept":
        db_connection.trade_delete(trade_uid=uid)
        msg = await send_message(client.get_channel(trade_doc['channel_id']), await get_message_data("Trade", "CancelledAccept", doc=trade_doc))
        await delete_message(msg, 10)
    elif mode == "runtime":
        db_connection.trade_delete(trade_uid=uid)
        msg = await send_message(client.get_channel(trade_doc['channel_id']), await get_message_data("Trade", "CancelledRuntime", doc=trade_doc))
        await delete_message(msg, 10)

##################################
#            FIGHTING            #
##################################


@client.group(aliases=['f'])
async def fight(ctx):
    """
    Main Fight Command. Prefix (including 'f') for all other commands. Automatically adds Characters in Users 'team' to the Fight Team. Starts Fight.
    :param ctx: Invocation Message Data. If Mentions not empty acts as Fight Start.
    :return: None
    """
    await delete_command(ctx.message)
    if ctx.invoked_subcommand is not None:
        return

    if len(ctx.message.mentions) == 0:
        await send_message(ctx.channel, "Fighting Help")
        return

    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    if ctx.message.mentions[0] == ctx.author:
        msg = await send_message(ctx.channel, await get_message_data("Trade", "SelfFight", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    if not db_connection.profile_check(ctx.message.mentions[0]):
        msg = await send_message(ctx.channel, await get_message_data("General", "NoProfile", users=[ctx.author, ctx.message.mentions[0]]))
        await delete_message(msg, 10)
        return

    fight_doc = db_connection.fight_get(user_id=ctx.author.id)
    if fight_doc is not None:
        if fight_doc['accepted']:
            await send_message(ctx.channel, await get_message_data("Fighting", "AlreadyFighting", users=[ctx.author], doc=fight_doc))
            return
        elif not fight_doc['accepted'] and fight_doc['u2'] == ctx.author.id:
            db_connection.fight_accept(fight_doc['uid'])
            await send_message(ctx.channel, await get_message_data("Fighting", "Accepted", users=[ctx.author], doc=fight_doc))
            await send_message(ctx.channel, "Show what they should do next")
            scheduler.add_job(fight_timeout, 'date', [fight_doc['uid'], "runtime"], run_date=datetime.datetime.fromtimestamp(RegularExtensions.epochnow() + 600), name=f"{fight_doc['uid']} - Fight 10 Minute Timeout")
            return
        else:
            await send_message(ctx.channel, await get_message_data("Fighting", "Started", users=[ctx.author], doc=fight_doc))
            return
    uid = db_connection.fight_create(ctx.message.mentions[0], ctx.channel)
    await send_message(ctx.channel, await get_message_data("Fighting", "Started", users=[ctx.author, ctx.message.mentions[0]]))
    scheduler.add_job(fight_timeout, 'date', [uid, "accept"], run_date=datetime.datetime.fromtimestamp(RegularExtensions.epochnow() + 180), name=f"{uid} - Fight Accept 3 Minute Timeout")


@fight.command()
async def abandon(ctx):
    """
    Trade Command to abandon a Fight. Automatically picks the Fight associated to the User if applicable. If Fight is running, asks if you'd like to give up.
    :param ctx: Invocation Message Data including the User
    """
    await delete_command(ctx.message)
    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    fight_doc = db_connection.fight_get(user_id=ctx.author.id)
    if fight_doc is None:
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "NotFighting", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    if not fight_doc['running']:
        db_connection.fight_delete(fight_doc['uid'])
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "Cancelled", users=[ctx.author], doc=fight_doc))
        await delete_message(msg, 10)
    else:
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "GiveUp", users=[ctx.author], doc=fight_doc))
        await add_emoji(msg, ["👍", "👎"])

        def message_check(m):
            return m.content.lower() in ['y', 'yes', 'ye', 'yeh', 'yus', 'n', 'no', 'nah', 'nay', 'nu'] and m.author == ctx.author

        def reaction_check(reaction, user):
            return str(reaction.emoji) in ["👍", "👎"] and user == ctx.author

        done, pending = await asyncio.wait([client.wait_for('message', check=message_check, timeout=20), client.wait_for('reaction_add', check=reaction_check, timeout=20)], return_when=asyncio.FIRST_COMPLETED)

        try:
            result = done.pop().result()
            for future in pending:
                future.cancel()
        except asyncio.TimeoutError:
            for future in pending:
                future.cancel()
            msg2 = await send_message(ctx.channel, await get_message_data("Fighting", "NotCancelledNoResponse", users=[ctx.author], doc=fight_doc))
            await delete_message(msg, 10)
            await delete_message(msg2, 10)
            return

        if (type(result) == discord.Message and any(s in result.content for s in ['y', 'yes', 'ye', 'yeh', 'yus'])) or (type(result) == tuple and result[0].emoji == "👍"):
            db_connection.fight_delete(fight_doc['uid'])
            msg2 = await send_message(ctx.channel, await get_message_data("Fighting", "Cancelled", users=[ctx.author], doc=fight_doc))
            await delete_message(msg, 10)
            await delete_message(msg2, 10)
        elif (type(result) == discord.Message and any(s in result.content for s in ['n', 'no', 'nah', 'nay', 'nu'])) or (type(result) == tuple and result[0].emoji == "👎"):
            msg2 = await send_message(ctx.channel, await get_message_data("Fighting", "NotCancelled", users=[ctx.author], doc=fight_doc))
            await delete_message(msg, 10)
            await delete_message(msg2, 10)


@fight.command()
async def accept(ctx, user: discord.Member = None):
    """
    Fight Command to accept a Fight. Automatically picks the Fight associated to the User if applicable.
    :param ctx: Invocation Message Data
    :param user: discord.Member that you want to accept the Fight from.
    """
    await delete_command(ctx.message)
    if user is None:
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "NoAcceptTarget", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    fight_doc = db_connection.fight_get(user_id=user.id)
    if fight_doc is None:
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "NotFightingWith", users=[ctx.author, ctx.message.mentions[0]]))
        await delete_message(msg, 10)
        return

    result = db_connection.fight_accept(uid=fight_doc['uid'])
    if result:
        await send_message(ctx.channel, await get_message_data("Fighting", "Accepted", users=[ctx.author], doc=fight_doc))
        await send_message(ctx.channel, "Show what they should do next")
        scheduler.add_job(fight_timeout, 'date', [fight_doc['uid'], "runtime"], run_date=datetime.datetime.fromtimestamp(RegularExtensions.epochnow() + 600), name=f"{fight_doc['uid']} - Fight 10 Minute Timeout")


@fight.command()
async def add(ctx, character_index=None):
    """
    Fight Command to add a Character to your Team.
    :param ctx: Invocation Message Data
    :param character_index: Local Index of the Character that you want to add
    :return: None
    """
    await delete_command(ctx.message)
    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    fight_doc = db_connection.fight_get(user_id=ctx.author.id)
    if fight_doc is None:
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "NotFighting", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    u1 = client.get_user(fight_doc['u1'])
    u2 = client.get_user(fight_doc['u2'])
    if RegularExtensions.intTryParse(character_index) is None:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "NoLocalIDAdd", users=[ctx.author], doc=fight_doc))
        await delete_message(msg, 10)
        return None

    result = db_connection.fight_add(int(character_index))
    if result is True:
        image = db_connection.fight_get_img(fight_doc['uid'], [u1.display_name, u2.display_name])
        msg = await send_message(ctx.channel, message=await get_message_data("Fighting", "FightAdd", users=[ctx.author], doc=fight_doc), file=image)
        if fight_doc['last_status'] is not None:
            try:
                await delete_message(await client.get_channel(fight_doc['last_status'][0]).get_message(fight_doc['last_status'][1]), 0)
            except discord.NotFound:
                pass
        db_connection.fight_update_status(fight_doc['uid'], [msg.channel.id, msg.id])
    elif result is IndexError:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "SlotEmptyDoc", users=[ctx.author], doc=fight_doc))
        await delete_message(msg, 10)
    elif result is False:
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "AlreadyInTeam", users=[ctx.author], doc=fight_doc))
        await delete_message(msg, 10)
    elif result is 3:
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "MaxThree", users=[ctx.author], doc=fight_doc))
        await delete_message(msg, 10)


@fight.command()
async def remove(ctx, character_index=None):
    """
    Fight Command to remove a Character from your Team.
    :param ctx: Invocation Message Data
    :param character_index: Local Index of the Character that you want to remove
    :return: None
    """
    await delete_command(ctx.message)
    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    fight_doc = db_connection.fight_get(user_id=ctx.author.id)
    if fight_doc is not None:
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "NotFighting", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    u1 = client.get_user(fight_doc['u1'])
    u2 = client.get_user(fight_doc['u2'])
    if RegularExtensions.intTryParse(character_index) is None:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "NoLocalIDRemove", users=[ctx.author], doc=fight_doc))
        await delete_message(msg, 10)
        return None

    result = db_connection.fight_add(int(character_index))
    if result is True:
        image = db_connection.fight_get_img(fight_doc['uid'], [u1.display_name, u2.display_name])
        msg = await send_message(ctx.channel, message=await get_message_data("Fighting", "FightRemove", users=[ctx.author], doc=fight_doc), file=image)
        if fight_doc['last_status'] is not None:
            try:
                await delete_message(await client.get_channel(fight_doc['last_status'][0]).get_message(fight_doc['last_status'][1]), 0)
            except discord.NotFound:
                pass
        db_connection.fight_update_status(fight_doc['uid'], [msg.channel.id, msg.id])
    elif result is IndexError:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "SlotEmptyDoc", users=[ctx.author], doc=fight_doc))
        await delete_message(msg, 10)
    elif result is False:
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "NotInTeam", users=[ctx.author], doc=fight_doc))
        await delete_message(msg, 10)


@fight.command()
async def ready(ctx):
    """
    Fight Command to set yourself to ready.
    :param ctx: Invocation Message Data
    """
    await delete_command(ctx.message)
    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    fight_doc = db_connection.fight_get(user_id=ctx.author.id)
    if fight_doc is None:
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "NotFighting", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    result = db_connection.fight_ready()
    if result is True:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "NowReady", users=[ctx.author], doc=fight_doc))
        await delete_message(msg, 10)
    elif result is False:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "AlreadyReady", users=[ctx.author], doc=fight_doc))
        await delete_message(msg, 10)
    elif result is "NoTeam":
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "NoTeamSet", users=[ctx.author], doc=fight_doc))
        await delete_message(msg, 10)
    elif result is "Start":
        await send_message(ctx.channel, await get_message_data("Shared", "NowReady", users=[ctx.author], doc=fight_doc))
        await send_message(ctx.channel, await get_message_data("Fighting", "idkyet", users=[ctx.author], doc=fight_doc))
        timer = 0
        while True:
            await asyncio.sleep(1)
            timer += 1
            fight_doc = db_connection.fight_get(fight_uid=fight_doc['uid'])
            if not (fight_doc['u1_ready'] and fight_doc['u2_ready']):
                await send_message(ctx.channel, await get_message_data("Fighting", "StartHalted", users=[ctx.author], doc=fight_doc))
                return
            if timer == 10:
                break

        db_connection.fight_start(fight_doc['uid'])
        scheduler.add_job(fight_timeout, 'date', [fight_doc['uid']], run_date=datetime.datetime.fromtimestamp(RegularExtensions.epochnow() + 600), name=f"{fight_doc['uid']} - Fight 10 Minute Timeout")
        await fight_main(fight_doc['uid'])


@fight.command()
async def unready(ctx):
    """
    Trade Command to set yourself as unready.
    :param ctx: Invocation Message Data
    """
    await delete_command(ctx.message)
    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    fight_doc = db_connection.fight_get(user_id=ctx.author.id)
    if fight_doc is None:
        msg = await send_message(ctx.channel, await get_message_data("Fighting", "NotFighting", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    result = db_connection.fight_unready()
    if result:
        await send_message(ctx.channel, await get_message_data("Shared", "NowUnready", users=[ctx.author], doc=fight_doc))
    elif not result:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "AlreadyUnready", users=[ctx.author], doc=fight_doc))
        await delete_message(msg, 10)


async def fight_main(uid):
    db_connection = WaifuLaifu()
    fight_doc = db_connection.fight_get(fight_uid=uid)
    fight_channel = client.get_channel(fight_doc['channel'])
    if fight_doc is None:
        return

    if fight_doc['turn'] == "u1":
        turn_user = client.get_user(fight_doc['u1'])
    elif fight_doc['turn'] == "u2":
        turn_user = client.get_user(fight_doc['u2'])

    msg = await get_fight_status_message(fight_doc, db_connection)
    status_message = await send_message(fight_channel, f"```{msg}```")
    await add_emoji(status_message, ['1⃣', '2⃣', '3⃣', '4⃣'])

    def message_check(m):
        return m.content.lower() in ['1', '2', '3', '4'] and m.author == turn_user

    def reaction_check(reaction, user):
        return str(reaction.emoji) in ['1⃣', '2⃣', '3⃣', '4⃣'] and user == turn_user

    done, pending = await asyncio.wait([client.wait_for('message', check=message_check, timeout=60), client.wait_for('reaction_add', check=reaction_check, timeout=60)], return_when=asyncio.FIRST_COMPLETED)

    try:
        result = done.pop().result()
        if type(result) == discord.Message:
            number = int(result.content) - 1
            await delete_message(result, 0)
        elif type(result) == tuple:
            if result[0].emoji == '1⃣':
                number = 0
            elif result[0].emoji == '2⃣':
                number = 1
            elif result[0].emoji == '3⃣':
                number = 2
            elif result[0].emoji == '4⃣':
                number = 3
        for future in pending:
            future.cancel()
    except asyncio.TimeoutError:
        fight_doc = db_connection.fight_get(fight_uid=uid)
        if fight_doc is None:
            return

        await send_message(fight_channel, await get_message_data("Fighting", "RandomAttack", users=[turn_user], doc=fight_doc))
        number = random.randint(0, 4)
        for future in pending:
            future.cancel()

    damage, critical, defeated = db_connection.fight_execute_attack(uid=uid, attack_id=number)
    if damage is not "Missed":
        result_message = ""
        if critical:
            result_message += f"{await get_message_data('Fighting', 'CriticalHit', users=[turn_user], doc=fight_doc)}\n"
        target = "u1" if fight_doc['turn'] == "u2" else "u2"
        result_message += f"{db_connection.get_character(fight_doc[f'{target}_team'][0]['id'])['name']} took {damage} Damage!"
        if defeated:
            result_message += f"\n{db_connection.get_character(fight_doc[f'{target}_team'][0]['id'])['name']} was defeated! "
            if len(fight_doc[f'{target}_team']) != 1:
                result_message += f"Next up is {db_connection.get_character(fight_doc[f'{target}_team'][1]['id'])['name']}!"
        result_message = await send_message(fight_channel, f"```{result_message}```")
    else:
        result_message = await send_message(fight_channel, await get_message_data("Fighting", "AttackMissed", users=[turn_user], doc=fight_doc))

    status = db_connection.fight_switch(fight_doc['uid'])
    if status is "Finished":
        print("te end")
    elif status is True:
        await delete_message(status_message, 0)
        await delete_message(result_message, 10)
        await fight_main(fight_doc['uid'])


async def fight_timeout(uid, mode):
    db_connection = WaifuLaifu()
    fight_doc = db_connection.fight_get(fight_uid=uid)
    if fight_doc is None:
        return

    if not fight_doc['accepted'] and mode == "accept":
        db_connection.fight_delete(fight_uid=uid)
        msg = await send_message(client.get_channel(fight_doc['channel_id']), await get_message_data("Fighting", "CancelledAccept", doc=fight_doc))
        await delete_message(msg, 10)
    elif mode == "runtime":
        db_connection.fight_delete(fight_uid=uid)
        msg = await send_message(client.get_channel(fight_doc['channel_id']), await get_message_data("Fighting", "CancelledRuntime", doc=fight_doc))
        await delete_message(msg, 10)


async def get_fight_status_message(fight_doc, connection):
    u1 = client.get_user(fight_doc['u1'])
    u2 = client.get_user(fight_doc['u2'])
    user = client.get_user(fight_doc[fight_doc['turn']])
    message_content_1 = [
        f"{u1.display_name}'s",
        f"{connection.get_character(fight_doc['u1_team'][0]['id'])['name']}",
        f"LVL    {fight_doc['u1_team'][0]['LVL']}",
        f"HP    {fight_doc['u1_team'][0]['HP']}",
        "",
        f"1. {connection.get_attack(fight_doc['u1_team'][0]['attacks'][0])['name']}",
        f"2. {connection.get_attack(fight_doc['u1_team'][0]['attacks'][1])['name']}",
        f"3. {connection.get_attack(fight_doc['u1_team'][0]['attacks'][2])['name']}",
        f"4. {connection.get_attack(fight_doc['u1_team'][0]['attacks'][3])['name']}"]
    message_content_2 = [
        f"{u2.display_name}'s",
        f"{connection.get_character(fight_doc['u2_team'][0]['id'])['name']}",
        f"LVL    {fight_doc['u2_team'][0]['LVL']}",
        f"HP    {fight_doc['u2_team'][0]['HP']}",
        "",
        f"1. {connection.get_attack(fight_doc['u2_team'][0]['attacks'][0])['name']}",
        f"2. {connection.get_attack(fight_doc['u2_team'][0]['attacks'][1])['name']}",
        f"3. {connection.get_attack(fight_doc['u2_team'][0]['attacks'][2])['name']}",
        f"4. {connection.get_attack(fight_doc['u2_team'][0]['attacks'][3])['name']}"]
    longest_message = ""
    for message_part in message_content_1:
        if len(message_part) > len(longest_message):
            longest_message = message_part
    final_message = f"It's {user.display_name}'s Turn!\n\n"
    for i in range(len(message_content_1)):
        message_content_1[i] += (len(longest_message) - len(message_content_1[i])) * " "
        final_message += f"{message_content_1[i]} | {message_content_2[i]}\n"
    return final_message

##################################
#          ADVENTURES            #
##################################


@client.group(aliases=['a', 'adv'])
async def adventure(ctx, local_id=None):
    """
    Main Adventure Command. Prefix (including 'a' and 'adv') for all other commands.
    Functions as a shortcut to !adventure start & check depending on the availability of an Adventure Doc.

    :param local_id: Local ID of the Character you want to check / start an Adventure with.
    :param ctx: Invocation Message Data. If Mentions not empty acts as Fight Start.
    :return: None
    """
    await delete_command(ctx.message)
    if ctx.invoked_subcommand is not None:
        return

    db_connection = WaifuLaifu(user=ctx.author)
    if RegularExtensions.intTryParse(local_id) is None:
        msg = await send_message(ctx.author, await get_message_data("Adventure", "StartNoLocalID", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    adventure_doc = db_connection.adventure_get(int(local_id))
    if adventure_doc is None:
        db_connection.close()
        await start(ctx, local_id)  # --> Start Adventure
    else:
        name = f"{adventure_doc['character']['nickname']} ({db_connection.get_character(adventure_doc['character']['id'])['name']})" if adventure_doc['character']['nickname'] is not None else db_connection.get_character(adventure_doc['character']['id'])['name']
        embed = discord.Embed(title=f"{name}'s Adventure", color=get_color(ctx), description=f"Adventuring for: {RegularExtensions.get_time_difference(adventure_doc['stats']['start_time'])}\nFights: {adventure_doc['stats']['fights']}\nEvents: {adventure_doc['stats']['events']}\nGems earned: {adventure_doc['rewards']['gems']}\nEXP gained: {adventure_doc['rewards']['exp']}")
        await send_message(ctx.channel, embed=embed)


@adventure.command()
async def start(ctx, local_id=None):
    """
    Adventure Command. Starts an Adventure with the Character at the specified Local ID if slots available.
    :param ctx: Invocation Message Data
    :param local_id: Local ID of the Character you want to send on an Adventure.
    :return: None
    """
    db_connection = WaifuLaifu(user=ctx.author)
    if RegularExtensions.intTryParse(local_id) is None:
        msg = await send_message(ctx.author, await get_message_data("Adventure", "StartNoLocalID", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    character_doc = db_connection.get_local_character(int(local_id))
    name = db_connection.get_character(character_doc['id'])['name'] if character_doc['nickname'] is None else character_doc['nickname']
    result = db_connection.adventure_start(int(local_id))
    if result is "NoCharacter":
        msg = await send_message(ctx.channel, await get_message_data("Shared", "SlotEmpty", users=[ctx.author]))
        await delete_message(msg, 10)
    elif result is "Unavailable":
        msg = await send_message(ctx.channel, await get_message_data("Adventure", "Unavailable", values=name, users=[ctx.author]))
        await delete_message(msg, 10)
    else:
        await send_message(ctx.channel, await get_message_data("Adventure", "Started", values=name, users=[ctx.author]))
        scheduler.add_job(adventure_event, 'date', [result['uid']], run_date=datetime.datetime.fromtimestamp(result['next_event']), name=f"{result['uid']} - Adventure Next Event")


@adventure.command()
async def stop(ctx, local_id=None):
    """
    Adventure Command. Stops the running Adventure for the specified Local ID.
    :param ctx: Invocation Message Data
    :param local_id: Local ID of the Character.
    :return:
    """
    db_connection = WaifuLaifu(user=ctx.author)
    if RegularExtensions.intTryParse(local_id) is None:
        error_msg = await send_message(ctx.author, await get_message_data("Adventure", "StopNoLocalID", users=[ctx.author]))
        await delete_message(error_msg, 10)
        return

    character_doc = db_connection.get_local_character(local_id)
    status, gems, exp = db_connection.adventure_stop(int(local_id))
    if status is "NoCharacter":
        error_msg = await send_message(ctx.author, await get_message_data("Shared", "SlotEmpty", users=[ctx.author]))
        await delete_message(error_msg, 10)
    elif status is "NoDoc":
        error_msg = await send_message(ctx.author, await get_message_data("Adventure", "NoAdventure", users=[ctx.author]))
        await delete_message(error_msg, 10)
    elif status is True:
        await send_message(ctx.author, await get_message_data("Adventure", "Completed", values=[db_connection.get_character(character_doc['id'])['name'] if character_doc['nickname'] is None else character_doc['nickname'], gems, exp], users=[ctx.author]))


@adventure.command()
async def check(ctx, local_id=None):
    """
    Adventure Command. Returns the status of the specified Characters Adventure
    :param ctx: Invocation Message Data
    :param local_id: Local ID of the Character you want to check
    """
    if RegularExtensions.intTryParse(local_id) is None:
        error_msg = await send_message(ctx.channel, await get_message_data("Adventure", "CheckNoLocalID", users=[ctx.author]))
        await delete_message(error_msg, 10)
        return

    db_connection = WaifuLaifu(user=ctx.author)
    adventure_doc = db_connection.adventure_get(local_id=local_id)
    embed = discord.Embed(color=get_color(ctx), description='\n'.join(f"Adventuring for: {RegularExtensions.get_time_difference(adventure_doc['stats']['start_time'])}\nFights: {adventure_doc['stats']['fights']}\nEvents: {adventure_doc['stats']['events']}\nGems earned: {adventure_doc['rewards']['gems']}\nEXP gained: {adventure_doc['rewards']['exp']}"))
    await send_message(ctx.channel, embed=embed)


async def adventure_event(uid):
    db_connection = WaifuLaifu()
    await adventure_fight(uid)


async def adventure_fight(uid):
    db_connection = WaifuLaifu()
    adventure_doc = db_connection.adventure_db.find_one({'uid': uid})

    user = client.get_user(adventure_doc['user'])
    db_connection.load_user(user)

    name = db_connection.get_character(adventure_doc['character']['id'])['name'] if adventure_doc['character']['nickname'] is None else adventure_doc['character']['nickname']

    result, log = db_connection.adventure_start_fight(uid)
    if db_connection.check_setting("adventure_log"):
        embed = discord.Embed(description=f"{log.replace('$cname$', name)}", color=get_color())
        await send_message(user, embed=embed)
    if result:
        next_event = db_connection.adventure_update(uid)
        scheduler.add_job(adventure_event, 'date', [uid], run_date=datetime.datetime.fromtimestamp(next_event), name=f"{uid} - Adventure Next Event", misfire_grace_time=300)
    elif not result:
        status, gems, exp = db_connection.adventure_stop(uid=uid)
        await send_message(user, await get_message_data("Adventure", "Defeated", users=[user]))
        await send_message(user, await get_message_data("Adventure", "DefeatedResults", values=[name, gems, exp], users=[user]))


##################################
#           CLAIMING             #
##################################

async def spawn_character(ctx):
    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)

    character = db_connection.get_claim_character()

    series_name = character['origin'].split(" ")
    for i in range(len(series_name)):
        series_name[i] = series_name[i].replace(series_name[i][1:], (len(series_name[i])-1)*"x")
    character_name = character['name'].split(" ")
    print(" ".join(character_name))
    for i in range(len(character_name)):
        character_name[i] = character_name[i][0] + "."

    character_embed = discord.Embed(color=get_color(ctx))
    character_embed.add_field(name=await get_message_data("Claiming", "Appeared", values=character['gender'].replace('f', 'Waifu').replace('m', 'Husbando')), value=f"Character's initials are: {' '.join(character_name)}\nFrom {' '.join(series_name)}", inline=False)
    character_embed.set_image(url=character['picture'])
    character_embed.set_footer(text=await get_message_data("Claiming", "GuessExplain"))

    spawn_channel = discord.utils.find(lambda c: c.name == db_connection.server_document['settings']['spawn_channel'], ctx.guild.channels)
    spawn_message = await send_message(spawn_channel, embed=character_embed)

    removed_character, pushed_characters = db_connection.set_current_claim(spawn_message.id, character['name'], character['id'])

    if removed_character is not None:
        spawn_message = await get_message(spawn_channel, removed_character['m_id'])
        if spawn_message is not None:
            character = db_connection.get_claim_info(removed_character['c_id'])
            old_embed = spawn_message.embeds[0]
            new_embed = discord.Embed(color=get_color(ctx))
            new_embed.set_author(name=await get_message_data("Claiming", "Fled", values=character['gender'].replace('f', 'Waifu').replace('m', 'Husbando')))
            new_embed.set_image(url=old_embed.image.url)
            await spawn_message.edit(embed=new_embed)
    for character in pushed_characters:
        if character['claimed']:
            continue
        spawn_message = await get_message(spawn_channel, character['m_id'])
        if spawn_message is None:
            continue
        old_embed = spawn_message.embeds[0]
        if len(old_embed.fields) != 0 and old_embed.footer.text is not None:
            character = db_connection.get_claim_info(character['c_id'])
            new_embed = discord.Embed(color=get_color(ctx))
            new_embed.set_footer(text=old_embed.footer.text)
            print(character['name'])
            new_embed.add_field(name=await get_message_data("Claiming", "PremiumNow", values=character['gender'].replace('f', 'Waifu').replace('m', 'Husbando')), value=old_embed.fields[0].value)
            new_embed.set_image(url=old_embed.image.url)
            await spawn_message.edit(embed=new_embed)


@client.command()
async def claim(ctx, *, data):
    """
    Claim command. Attempts to claim a Character. If data is correct, adds the Character to the Users Collection.
    """
    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    if ctx.channel.name != db_connection.server_document['settings']['spawn_channel']:
        return

    match_status, data, premium = db_connection.check_claim_match(data=data)
    if match_status is True:
        character, set_data = db_connection.add_claim_character(data['c_id'])
        character_embed = discord.Embed(color=get_color(ctx))
        character_embed.add_field(name=await get_message_data("Claiming", "Claimed", values=character['gender'].replace('f', 'Waifu').replace('m', 'Husbando'), users=[ctx.author]), value=await get_message_data("Claiming", "Claimed", values=[character['name'], character['origin']], users=[ctx.author]))
        character_embed.set_image(url=character['picture'])

        db_connection.set_claimed(data['c_id'])
        spawn_message = await get_message(ctx.channel, data['m_id'])
        await spawn_message.edit(embed=character_embed)

        spawn_channel = discord.utils.find(lambda c: c.name == db_connection.server_document['settings']['spawn_channel'], ctx.guild.channels)
        await send_message(spawn_channel, await get_message_data("Claiming", "ClaimedNotice", values=character['gender'].replace('f', 'Waifu').replace('m', 'Husbando'), users=[ctx.author]))

        if set_data is not None:
            for card_set in set_data:
                if card_set[1] == 0.5:
                    await send_message(ctx.author, await get_message_data("Sets", "Half", values=card_set[0], users=[ctx.author]))
                elif card_set[1] == 1:
                    await send_message(ctx.author, await get_message_data("Sets", "Complete", values=card_set[0], users=[ctx.author]))

    else:
        gender_value = db_connection.get_character(data['c_id'] if type(data) == list else data[0]['c_id'])['gender'].replace('f', 'Waifu').replace('m', 'Husbando')
        if match_status is "Claimed":
            await send_message(ctx.channel, await get_message_data("Claiming", "AlreadyClaimed", users=[ctx.author], values=gender_value))
        elif match_status is "NotPremium":
            await send_message(ctx.channel, await get_message_data("Claiming", "PremiumOnly", users=[ctx.author], values=gender_value))
        elif match_status is IndexError:
            await send_message(ctx.channel, await get_message_data("Claiming", "NoTarget", users=[ctx.author], values=gender_value))
        elif match_status is False:
            if premium:
                await send_message(ctx.channel, await get_message_data("Claiming", "MissPremium", users=[ctx.author], values=[data, gender_value]))
            else:
                await send_message(ctx.channel, await get_message_data("Claiming", "Miss", users=[ctx.author], values=[data, gender_value]))


##################################
#         USER INVENTORY         #
##################################

@client.command(aliases=['i'])
async def inventory(ctx, *, data=None):
    """
    Displays the Inventory of the User.
    :param ctx: Invocation Message Data.
    :param data: Extra Filters (-s, -o, -w, -f...)
    """
    data_split = []
    if data is not None:
        data_split = data.split(" -")
        for i in range(len(data_split)-1):
            data_split[i+1] = f"-{data_split[i+1]}"
    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    character_list, series_data, filters, order = db_connection.get_inventory(data_split)

    search_query = []
    if "name" in filters:
        search_query.append(f'"{filters["name"].title()}"')
    if "origin" in filters:
        search_query.append(f"From {filters['origin'].title()}")

    if data is not None and "-extended" in data:
        pages = math.ceil(len(character_list) / 20)
        if order['page'] < pages:
            used_characters = character_list[20 * (order['page']-1):][:20]
        else:
            order['page'] = pages
            used_characters = character_list[20 * (pages - 1):][:20]
        string_array = []
        for character in used_characters:
            string_array.append([f"{character['local_id']}", f"{character['id']}", f"[{character['rating']}]", f"{character['name']}", f"LVL {character['LVL']}", f"Affection {character['Affection']}"])
        string_array = '\n'.join(RegularExtensions.string_extender(string_array, " | "))
        embed = discord.Embed(title=f"Inventory Lookup: {' | '.join(search_query)}", description=string_array, color=get_color(ctx))
    else:
        pages = math.ceil(len(series_data) / 5)
        if order['page'] < pages:
            used_series = series_data[5 * (order['page']-1):][:5]
        else:
            order['page'] = pages
            used_series = series_data[5 * (pages - 1):][:5]
        embed = discord.Embed(title=f"Inventory Lookup: {' | '.join(search_query)}", color=get_color(ctx))
        for series in used_series:
            string = ""
            for character in series['characters']:
                string += f"{character['local_id']} | {character['name']} | {character['id']}\n"
            embed.add_field(name=f"{series['name']} {len(series['characters'])} / {series['character_amount']}", value=string, inline=False)
    embed.set_footer(text=f"Page {order['page']} / {pages} | Add -p [page] to go to a specific page!")
    await send_message(ctx.channel, embed=embed)


@client.command()
async def lookup(ctx, *, data=None):
    """
    Lookup of the specified Data. Returns a List of Characters.
    :param ctx: Invocation Message Data
    :param data: Filters (-s, -o, -w, -f)
    """
    data_split = []
    if data is not None:
        data_split = data.split(" -")
        for i in range(len(data_split) - 1):
            data_split[i + 1] = f"-{data_split[i + 1]}"
    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    character_list, series_data, filters, order = db_connection.get_lookup(data_split)

    search_query = []
    if "name" in filters:
        search_query.append(f'"{filters["name"].title()}"')
    if "origin" in filters:
        search_query.append(f"From {filters['origin'].title()}")

    pages = math.ceil(len(series_data) / 5)
    if order['page'] < pages:
        used_series = series_data[5 * (order['page'] - 1):][:5]
    else:
        order['page'] = pages
        used_series = series_data[5 * (pages - 1):][:5]
    embed = discord.Embed(title=f"Character Lookup: {' | '.join(search_query)}", color=get_color(ctx))
    for series in used_series:
        string = ""
        if len(filters) == 0:
            if len(series['characters']) > 5:
                for i in range(5):
                    string += f"{series['characters'][i]['id']} | {series['characters'][i]['name']}\n"
                string += f"[{len(series['characters'])-5} more Characters]"
            else:
                for i in range(len(series['characters'])):
                    string += f"{series['characters'][i]['id']} | {series['characters'][i]['name']}\n"
        else:
            for character in series['characters']:
                string += f"{character['id']} | {character['name']}\n"
        embed.add_field(name=f"{series['name']}", value=string, inline=False)
    embed.set_footer(text=f"Page {order['page']} / {pages} | Add -p [page] to go to a specific page!")
    await send_message(ctx.channel, embed=embed)


@client.command()
async def view(ctx, *, data=""):  # TODO Make work with Character Name
    """
    Views the Profile of a Character.
    :param ctx: Invocation Message Data. If Mentions not empty checks Characters of the specified User.
    :param data: Character ID
    :return:
    """
    if len(ctx.message.mentions) != 0:
        user = ctx.message.mentions[0]
        for mention in ctx.message.mentions:
            data = data.replace(mention.mention, "").lstrip()
    else:
        user = ctx.author

    data = RegularExtensions.intTryParse(data)
    if data is None:
        msg = await send_message(ctx.channel, await get_message_data("General", "NoID"))
        await delete_message(msg, 10)
        return

    db_connection = WaifuLaifu(server=ctx.guild)
    if db_connection.profile_check(user) or user is ctx.author:
        db_connection.load_user(user)
    else:
        msg = await send_message(ctx.channel, await get_message_data("General", "NoProfile", users=[ctx.author, ctx.message.mentions[0]]))
        await delete_message(msg, 10)
        return

    if user is not ctx.author:
        if not db_connection.user_document['inventory_public']:
            msg = await send_message(ctx.channel, await get_message_data("Settings", "InventoryNotPublic", users=[ctx.author, ctx.message.mentions[0]]))
            await delete_message(msg, 10)
            return

    character, character_doc = db_connection.get_view(data)
    if character is None:
        return

    if character['nickname'] is not None:
        name = character['nickname']
    else:
        name = character_doc['name']
    character_embed = discord.Embed(title=name, description=f"Claimed by {user.mention}\nLocal ID: {character['local_id']}\nCharacter ID: {character['id']}\nRating: {character['rating']}\n\nOrigin: {character_doc['origin']}\nGender: {character_doc['gender'].replace('m', 'Male').replace('f', 'Female')}\n\nLVL: {character['LVL']}\nHP: {character['HP']}\nATK: {character['ATK']}\nDEF: {character['DEF']}\n\nAffection: {character['Affection']}", color=get_color(ctx))
    character_embed.set_image(url=character_doc['picture'])
    await send_message(ctx.channel, embed=character_embed)


### Nicknames ###


@client.group(aliases=['nick', 'nickname'])
async def alias(ctx):
    """
    Main Alias Command. Needed as a prefix (Including 'nick', 'nickname') for all other Alias Commands.
    :param ctx:
    """
    if ctx.invoked_subcommand is None:
        await send_message(ctx.channel, "Alias Help")


@alias.command(aliases=['s'])
async def set(ctx, local_id=None, *, name=None):
    """
    Alias Command. Sets the Alias / Nickname of the Character at the specified Local ID.
    :param ctx: Invocation Message Data.
    :param local_id: Local ID of the Character of which you want to set the Alias.
    :param name: Nickname
    :return: None
    """
    if RegularExtensions.intTryParse(local_id) is None:
        await send_message(ctx.channel, await get_message_data("Alias", "NoLocalID", users=[ctx.author]))
        return None

    if name is None:
        await send_message(ctx.channel, await get_message_data("Alias", "NotGiven", users=[ctx.author]))
        return None

    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    result, character_name = db_connection.nickname_set(local_id=int(local_id), name=name)
    if result is True:
        await send_message(ctx.channel, await get_message_data("Alias", "Set", values=[character_name, name], users=[ctx.author]))
    elif result is IndexError:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "SlotEmpty", users=[ctx.author]))
        await delete_message(msg, 10)


@alias.command(aliases=['r', 'res'])
async def reset(ctx, local_id=None):
    """
    Alias Command. Removes the Nickname of a Character.
    :param ctx: Invocation Message Data.
    :param local_id: Local ID of the Character whose Nickname you want to remove.
    """
    if RegularExtensions.intTryParse(local_id) is None:
        await send_message(ctx.channel, await get_message_data("Alias", "NoLocalID", users=[ctx.author]))
        return None

    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    result, character_name = db_connection.nickname_reset(local_id=int(local_id))
    if result is True:
        await send_message(ctx.channel, await get_message_data("Alias", "Reset", values=character_name, users=[ctx.author]))
    elif result is False:
        msg = await send_message(ctx.channel, await get_message_data("Alias", "NotSet", users=[ctx.author]))
        await delete_message(msg, 10)
    elif result is IndexError:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "SlotEmpty", users=[ctx.author]))
        await delete_message(msg, 10)

### Teams ###


@client.group()
async def team(ctx):
    """
    Main Team Command. Needed as a prefix for all other Team Commands.
    :param ctx: Invocation Message Data.
    """
    if ctx.invoked_subcommand is not None:
        return

    db_connection = WaifuLaifu(user=ctx.author)
    team_data = db_connection.team_get_list()

    string = ""
    waifu_list = []
    for i in range(len(team_data)):
        local_doc = db_connection.get_local_character(team_data[i]['local_id'])
        waifu_doc = db_connection.get_character(local_doc['id'])
        name = f"{local_doc['nickname']} ({waifu_doc['name']})" if local_doc['nickname'] is not None else waifu_doc['name']  # TODO Somehwere error :)
        string += f"{local_doc['local_id']} | **{name}** | LVL {local_doc['LVL']} | HP {local_doc['HP']} | ATK {local_doc['ATK']} | DEF {local_doc['DEF']}\n"

    string = '-' * len(waifu_list[0]) + "\n" + string

    await send_message(ctx.channel, string)
    embed = discord.Embed(description=f"**{'-' * len(waifu_list[0])}**\n{string}", title="Your current Team:")
    await send_message(ctx.channel, embed=embed)


@team.command(aliases=['a'])
async def add(ctx, local_id=None):
    """
    Team Command. Adds the Character at the specified Local ID to the Users predefined Team.
    :param ctx: Invocation Message Data.
    :param local_id: Local ID of the Character you want to add.
    :return: None
    """
    if not RegularExtensions.intTryParse(local_id):
        error_msg = await send_message(ctx.channel, await get_message_data("Team", "NoLocalID", users=[ctx.author]))
        await delete_message(error_msg, 10)
        return

    db_connection = WaifuLaifu(user=ctx.author)
    result, name_data = db_connection.team_add(local_id)
    if result is None:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "SlotEmpty", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    name = f"{name_data[1]} ({name_data[0]})" if name_data[1] else name_data[0]
    if result is "Duplicate":
        error_msg = await send_message(ctx.channel, await get_message_data("Team", "Duplicate", values=name, users=[ctx.author]))
        await delete_message(error_msg, 10)
    elif result is "Full":
        error_msg = await send_message(ctx.channel, await get_message_data("Team", "Full", values=name, users=[ctx.author]))
        await delete_message(error_msg, 10)
    elif result is True:
        await send_message(ctx.channel, await get_message_data("Team", "Added", values=name, users=[ctx.author]))


@team.command(aliases=['r', 'rem'])
async def remove(ctx, local_id=None):
    """
    Team Command. Removes the Character at the specified Local ID from the predefined Team.
    :param ctx: Invocation Message Data.
    :param local_id: Local ID of the Character you want to remove.
    """
    if not RegularExtensions.intTryParse(local_id):
        error_msg = await send_message(ctx.channel, await get_message_data("Team", "NoLocalID", users=[ctx.author]))
        await delete_message(error_msg, 10)
        return

    db_connection = WaifuLaifu(user=ctx.author)
    result, name_data = db_connection.team_remove(local_id)
    if result is None:
        msg = await send_message(ctx.channel, await get_message_data("Shared", "SlotEmpty", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    name = f"{name_data[1]} ({name_data[0]})" if name_data[1] else name_data[0]
    if result is False:
        error_msg = await send_message(ctx.channel, await get_message_data("Team", "NotInTeam", values=name, users=[ctx.author]))
        await delete_message(error_msg, 10)
    elif result is True:
        await send_message(ctx.channel, await get_message_data("Team", "Removed", values=name, users=[ctx.author]))

##################################
#          INTERACTING           #
##################################


@client.command(alias=['int'])
async def interact(ctx, local_id=None):  # TODO Make Array for these Messages
    """
    Interaction Command. Alternative 'int'. Interacts with the Character at the specified Local ID to increase its Affection.
    :param ctx: Invocation Message Data.
    :param local_id: Local ID of the Character you'd like to interact with.
    """
    local_id = RegularExtensions.intTryParse(local_id)
    if local_id is None:
        err = await send_message(ctx.channel, await get_message_data("Interact", "NoLocalID", users=[ctx.author]))
        await delete_message(err, 10)
        return

    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    character, additional_info = db_connection.interact(local_id)
    if character is not None:
        embed = discord.Embed(title="Interaction", description=f"You do the interacting magic with {character['doc']['name']}.\n{character['doc']['gender'].replace('f', 'Her').replace('m', 'His')} Affection rose by {additional_info} Points!", color=get_color(ctx))
        embed.set_thumbnail(url=character['doc']['picture'])
        await send_message(ctx.channel, embed=embed)
        return

    if additional_info[0] is "Cooldown":
        embed = discord.Embed(title="Interaction", description=f"{additional_info[1]['doc']['name']} is currently getting groceries.\n{additional_info[1]['doc']['gender'].replace('f', 'She').replace('m','He')} should be back in {RegularExtensions.get_time_difference(end=additional_info[1]['interaction_cooldown'])}!", color=get_color(ctx))
        embed.set_thumbnail(url=additional_info[1]['doc']['picture'])
        await send_message(ctx.channel, embed=embed)
    elif additional_info[0] is "Max":
        embed = discord.Embed(title="Interaction", description=f"Ur relationship with {additional_info[1]['doc']['name']} is already maxed out.\n{additional_info[1]['doc']['gender'].replace('f', 'She').replace('m', 'He')} a happy {additional_info[1]['doc']['gender'].replace('f', 'Girl').replace('m', 'Boy')}!", color=get_color(ctx))
        embed.set_thumbnail(url=additional_info[1]['doc']['picture'])
        await send_message(ctx.channel, embed=embed)
    elif additional_info[0] is "NotFound":
        msg = await send_message(ctx.channel, await get_message_data("Interact", "LocalIDNotFound", users=[ctx.author]))
        await delete_message(msg, 10)


##################################
#              SHOP              #
##################################

current_shop_users = []


@client.group(aliases=['s'])
async def shop(ctx):
    global current_shop_users
    await delete_command(ctx.message)
    if ctx.author.id in current_shop_users:
        return
    current_shop_users.append(ctx.author.id)
    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    shop_list = db_connection.shop_get()

    string_array = []
    for item in range(len(shop_list)):
        item_doc = db_connection.item_get(shop_list[item]['id'])
        string_array.append([f"{item+1}", f"{item_doc['name']}", f"{item_doc['price']} Gems"])
    string = '\n'.join(RegularExtensions.string_extender(string_array, " | "))

    embed = discord.Embed(title=f"{ctx.guild.name}'s Item Shop", description=string, color=get_color(ctx))
    msg = await send_message(ctx.channel, embed=embed)

    reaction_list = ['1⃣', '2⃣', '3⃣', '4⃣', '5⃣']
    await add_emoji(msg, reaction_list[:len(shop_list)])

    while True:
        def msg_check(message):
            return message.author == ctx.author and message.channel == ctx.channel and message.content[:1] in ["1", "2", "3", "4", "5"][:len(shop_list)]

        def reaction_check(reaction, reaction_user):
            return str(reaction.emoji) in reaction_list[:len(shop_list)] and reaction_user == ctx.author and reaction.message.id == msg.id

        done, pending = await asyncio.wait([client.wait_for('message', check=msg_check, timeout=30), client.wait_for('reaction_add', check=reaction_check, timeout=30)], return_when=asyncio.FIRST_COMPLETED)

        try:
            response = done.pop().result()
            for future in pending:
                future.cancel()
        except asyncio.TimeoutError:
            for future in pending:
                future.cancel()
            for future in done:
                future.cancel()
            await delete_message(msg, 0)
            current_shop_users.remove(ctx.author.id)
            return

        item = None

        if type(response) == tuple:
            for i in range(len(reaction_list)):
                if response[0].emoji == reaction_list[i]:
                    item = shop_list[i]
        elif type(response) == discord.Message:
            item = shop_list[int(response[:1]) - 1]

        result = db_connection.shop_buy(item['id'])
        if result == "NotEnoughGems":
            await send_message(ctx.channel, await get_message_data("Shop", "TooExpensive", users=[ctx.author]))
        elif result:
            await send_message(ctx.channel, await get_message_data("Shop", "Bought", users=[ctx.author], values=[item['name'], item['price']]))


async def refresh_shop():
    db_connection = WaifuLaifu()
    result = db_connection.shops_refresh()
    if result:
        gen_log("Shops successfully refreshed!")


##################################
#          ITEM USAGE            #
##################################

@client.command(aliases=['b'])
async def boosters(ctx):
    db_connection = WaifuLaifu(user=ctx.author)
    booster_data = db_connection.boosters_get()
    # TODO Display Effects? If so how?
    embed = discord.Embed(title=f"Boosters of {ctx.author.display_name}", color=get_color(ctx))


##################################
#     WISHLIST / FAVORITES       #
##################################

@client.group(aliases=['w', 'wish'])
async def wishlist(ctx, page=1):
    if ctx.invoked_subcommand is not None:
        return

    user = ctx.author
    db_connection = WaifuLaifu(user=ctx.author)
    if len(ctx.message.mentions) != 0:
        if not db_connection.profile_check(ctx.message.mentions[0]):
            await send_message(ctx.channel, await get_message_data("General", "NoProfile", users=[ctx.author, ctx.message.mentions[0]]))
            return

        db_connection.load_user(ctx.message.mentions[0])
        if not db_connection.check_setting("wishlist_public"):
            await send_message(ctx.channel, await get_message_data("Settings", "WishlistNotPublic", users=[ctx.author, ctx.message.mentions[0]]))
            return

        user = ctx.message.mentions[0]
    wishlist_data = db_connection.wishlist_get()

    pages = math.ceil(len(wishlist_data) / 10)
    if page > pages:
        page = pages

    wishlist_data = wishlist_data[(page - 1) * 10:][:10]

    wishlist_string = ""
    for character in wishlist_data:
        wishlist_string += f"{character['position']} | {character['id']} | {character['name']}\n"
    embed = discord.Embed(title=f"{user.display_name}'s Wishlist", color=get_color(ctx), description=wishlist_string)
    embed.set_footer(text=f"Page {page} / {pages}")
    await send_message(ctx.channel, embed=embed)


@wishlist.command(aliases=['a'])
async def add(ctx, *, data=None):
    if data is None:
        msg = await send_message(ctx.channel, await get_message_data("Wishlist", "NoLocalIDAdd", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    db_connection = WaifuLaifu(user=ctx.author)
    result, character = db_connection.wishlist_add(data)
    if result == "InList":
        await send_message(ctx.channel, await get_message_data("Wishlist", "AlreadyExists", users=[ctx.author], values=character))
    elif result == "Owned":
        await send_message(ctx.channel, await get_message_data("Wishlist", "AlreadyOwn", users=[ctx.author], values=character))
    elif result == "NotFound":
        await send_message(ctx.channel, await get_message_data("Wishlist", "NotFound", users=[ctx.author]))
    elif result is True:
        await send_message(ctx.channel, await get_message_data("Wishlist", "Added", users=[ctx.author], values=character))


@wishlist.command(aliases=['r', 'rem'])
async def remove(ctx, *, data=None):
    if data is None:
        msg = await send_message(ctx.channel, await get_message_data("Wishlist", "NoLocalIDRemove", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    db_connection = WaifuLaifu(user=ctx.author)
    result, character = db_connection.wishlist_remove(data)
    if result == "NotFound":
        await send_message(ctx.channel, await get_message_data("Wishlist", "NotOnWishlist", users=[ctx.author]))
    elif result is True:
        await send_message(ctx.channel, await get_message_data("Wishlist", "Removed", users=[ctx.author], values=character))


@wishlist.command(aliases=['s'])
async def set(ctx, *, data=None):
    if data is None:
        msg = await send_message(ctx.channel, await get_message_data("Wishlist", "NoLocalIDPosition", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    db_connection = WaifuLaifu(user=ctx.author)
    result, character = db_connection.wishlist_set(data)
    if result == "SamePosition":
        error_msg = await send_message(ctx.channel, await get_message_data("Wishlist", "SamePosition", users=[ctx.author], values=character))
        await delete_message(error_msg, 10)
    elif result == "NoPosition":
        error_msg = await send_message(ctx.channel, await get_message_data("Wishlist", "NoPosition", users=[ctx.author]))
        await delete_message(error_msg, 10)
    elif result:
        await send_message(ctx.channel, await get_message_data("Wishlist", "PositionSet", users=[ctx.author], values=character))


@client.group(aliases=['fav'])
async def favorites(ctx, page=1):
    if ctx.invoked_subcommand is not None:
        return

    user = ctx.author
    db_connection = WaifuLaifu(user=ctx.author)
    if len(ctx.message.mentions) != 0:
        user = ctx.message.mentions[0]
        if not db_connection.profile_check(ctx.message.mentions[0]):
            await send_message(ctx.channel, await get_message_data("General", "NoProfile", users=[ctx.author, ctx.message.mentions[0]]))
            return
        db_connection.load_user(ctx.message.mentions[0])
        if not db_connection.check_setting("favorites_public"):
            await send_message(ctx.channel, await get_message_data("Settings", "FavoritesNotPublic", users=[ctx.author, ctx.message.mentions[0]]))
            return
    favorites_data = db_connection.favorites_get()

    pages = math.ceil(len(favorites_data) / 10)
    if page > pages:
        page = pages

    favorites_data = favorites_data[(page-1)*10:][:10]

    favorites_string = ""
    for character in favorites_data:
        favorites_string += f"{character['position']} | {character['id']} | {character['name']}\n"

    embed = discord.Embed(title=f"{user.display_name}'s Favourites", color=get_color(ctx), description=favorites_string)
    embed.set_footer(text=f"Page {page} / {pages}")
    await send_message(ctx.channel, embed=embed)


@favorites.command(aliases=['a'])
async def add(ctx, *, data=None):
    if data is None:
        msg = await send_message(ctx.channel, await get_message_data("Favorites", "NoLocalIDAdd", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    db_connection = WaifuLaifu(user=ctx.author)
    result, character = db_connection.favorites_add(data)
    if result == "InList":
        await send_message(ctx.channel, await get_message_data("Favorites", "AlreadyExists", users=[ctx.author], values=character))
    elif result == "NotFound":
        await send_message(ctx.channel, await get_message_data("Favorites", "NotFound", users=[ctx.author]))
    elif result is True:
        await send_message(ctx.channel, await get_message_data("Favorites", "Added", users=[ctx.author], values=character))


@favorites.command(aliases=['r', 'rem'])
async def remove(ctx, *, data=None):
    if data is None:
        msg = await send_message(ctx.channel, await get_message_data("Favorites", "NoLocalIDRemove", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    db_connection = WaifuLaifu(user=ctx.author)
    result, character = db_connection.favorites_remove(data)
    if result == "NotFound":
        await send_message(ctx.channel, await get_message_data("Favorites", "NotFound", users=[ctx.author]))
    elif result is True:
        await send_message(ctx.channel, await get_message_data("Favorites", "Removed", users=[ctx.author], values=character))


@favorites.command(aliases=['s'])
async def set(ctx, *, data=None):
    if data is None:
        msg = await send_message(ctx.channel, await get_message_data("Favorites", "NoLocalIDPosition", users=[ctx.author]))
        await delete_message(msg, 10)
        return

    db_connection = WaifuLaifu(user=ctx.author)
    result, character = db_connection.favorites_set(data)
    if result == "SamePosition":
        error_msg = await send_message(ctx.channel, await get_message_data("Favorites", "SamePosition", users=[ctx.author], values=character))
        await delete_message(error_msg, 10)
    elif result == "NoPosition":
        error_msg = await send_message(ctx.channel, await get_message_data("Favorites", "NoPosition", users=[ctx.author]))
        await delete_message(error_msg, 10)
    elif result:
        await send_message(ctx.channel, await get_message_data("Favorites", "PositionSet", users=[ctx.author], values=character))

##################################
#             INFO               #
##################################


@client.command()
async def invite(ctx):
    """
    Sends an invite URL for the Bot.
    :param ctx: Invocation Message Data.
    """
    await delete_command(ctx.message)
    await send_message(ctx.author, await get_message_data('Misc', 'Invite'))


@client.command()
async def patreon(ctx):
    """
    Sends the Patreon Link for the Bot.
    :param ctx: Invocation Message Data.
    """
    await delete_command(ctx.message)
    msg = await send_message(ctx.author, await get_message_data('Misc', 'Patreon'))


@client.command(aliases=['sinfo'])
async def serverinfo(ctx):
    """
    Sends Server Info.
    :param ctx: Invocation Message Data.
    """
    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    embed = discord.Embed(title="Server Info")
    embed.add_field(name="Current Prefix:", value=db_connection.server_document['settings']['command_prefix'])

    await send_message(ctx.channel, embed=embed)


@client.command()
async def shards(ctx):
    """
    Sends a Message with Shard Details if available.
    :param ctx: Invocation Message Data.
    """
    pass


@client.command(aliases=['binfo'])
async def botinfo(ctx):
    """
    Sends Bot Info.
    :param ctx: Invocation Message Data.
    """
    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)

    info_embed = discord.Embed(title=f"Running on Discord.py v{discord.__version__}", color=get_color(ctx))
    info_embed.set_author(name=f"WaifuBotto v{config_data['General']['Version']}", icon_url=ctx.me.avatar_url)
    info_embed.add_field(name="Ping", value=f"{int(client.latency * 1000)}ms")
    info_embed.add_field(name="Guilds", value=str(len(client.guilds)))
    info_embed.add_field(name="Characters", value=str(db_connection.character_db.count_documents(filter={})))
    info_embed.add_field(name="Users", value=str(db_connection.user_db.count_documents(filter={})))

    await send_message(ctx.channel, embed=info_embed)


##################################
#            SETTINGS            #
##################################

current_settings_users = []


@client.command()
async def settings(ctx):
    global current_settings_users
    await delete_command(message=ctx.message)
    if ctx.author.id in current_settings_users:
        return

    current_settings_users.append(ctx.author.id)
    db_connection = WaifuLaifu(user=ctx.author, server=ctx.guild)

    reaction_list = ['1⃣', '2⃣', '3⃣', '4⃣', '5⃣', '6⃣', '7⃣', '8⃣', '9⃣']
    msg = None
    while True:
        settings_list = db_connection.user_document['settings']
        if ctx.author.guild_permissions.manage_guild:
            settings_list = {**settings_list, **db_connection.server_document['settings']}

        string = ""
        counter = 1
        switch_counter = 0
        for name, value in settings_list.items():
            if type(value) is bool:
                string += f"{counter}. {name.replace('_', ' ').title()}: {'Enabled' if value else 'Disabled'}\n"
                switch_counter += 1
            elif type(value) in [int, str]:
                string += f"{counter}. {name.replace('_', ' ').title()}: {value}\n"
            counter += 1
        string += await get_message_data("Settings", "SettingSwitchExplanation", users=[ctx.author])
        embed = discord.Embed(color=get_color(ctx), description=string, title="User Settings")

        if msg is None:
            msg = await send_message(ctx.channel, embed=embed)
            await add_emoji(msg, reaction_list[:switch_counter])
        else:
            await msg.edit(embed=embed)

        def msg_check(message):
            return message.author == ctx.author and message.channel == ctx.channel and message.content[:1] in ["1", "2", "3", "4", "5", "6", "7", "8", "9"][:len(settings_list)]

        def reaction_check(reaction, reaction_user):
            return str(reaction.emoji) in reaction_list[:switch_counter] and reaction_user == ctx.author and reaction.message.id == msg.id

        done, pending = await asyncio.wait([client.wait_for('message', check=msg_check, timeout=30), client.wait_for('reaction_add', check=reaction_check, timeout=30)], return_when=asyncio.FIRST_COMPLETED)

        try:
            result = done.pop().result()
            for future in pending:
                future.cancel()
        except asyncio.TimeoutError:
            for future in pending:
                future.cancel()
            for future in done:
                future.cancel()
            await delete_message(msg, 0)
            current_settings_users.remove(ctx.author.id)
            return

        if type(result) == tuple:
            for i in range(len(reaction_list)):
                if result[0].emoji == reaction_list[i]:
                    db_connection.switch_setting(key=list(settings_list.keys())[i], value=not settings_list.get(list(settings_list.keys())[i]))
                    break
        elif type(result) == discord.Message:
            key = list(settings_list.keys())[int(result.content[:1]) - 1]
            if key == "spawn_channel":
                if len(result.channel_mentions) != 0:
                    db_connection.set_spawnchannel(result.channel_mentions[0])
                    await send_message(ctx.channel, await get_message_data("Settings", "SpawnChannelSet", users=[ctx.author], values=result.channel_mentions[0].mention))
                else:
                    await send_message(ctx.channel, await get_message_data("Settings", "NoSpawnChannel", users=[ctx.author]))
            elif key == "command_prefix":
                if len(result.content[2:]) != 0:
                    db_connection.set_prefix(result.content[2:])
                    await send_message(ctx.channel, await get_message_data("Settings", "CommandPrefixSet", users=[ctx.author], values=result.content[2:]))
                else:
                    await send_message(ctx.channel, await get_message_data("Settings", "NoCommandPrefix", users=[ctx.author]))
            else:
                db_connection.switch_setting(key=list(settings_list.keys())[int(result.content[:1]) - 1], value=not settings_list.get(list(settings_list.keys())[int(result.content[:1]) - 1]))


##################################
#             ADMIN              #
##################################

@client.command()
async def get_series(ctx, series_id=None, site="mal"):
    """
    Gets the Series Information from the Specified Site. Adds all Characters to the DB. Admin only.
    :param ctx: Invocation Message Data.
    :param series_id: ID of the Series
    :param site: Site the Series is on (MAL / VNDB available)
    """
    await delete_command(ctx.message)
    if not RegularExtensions.intTryParse(series_id):
        await send_message(ctx.author, f"**Please specify the ID of the Character you'd like to add!**")
    if permission_check(ctx.author.id):
        db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
        amount = db_connection.get_series_data(int(series_id), site)
        await send_message(ctx.author, f"**Added {amount} Characters!**")


@client.command()
async def get_character(ctx, character_id=None, site="mal"):
    """
    Gets the Character Information from the Specified Site. Adds Character to DB. Admin only.
    :param ctx: Invocation Message Data.
    :param character_id: ID of the Character on the specified Site.
    :param site: Site the Character is on (MAL / VNDB available)
    """
    await delete_command(ctx.message)
    if not RegularExtensions.intTryParse(character_id):
        await send_message(ctx.author, f"**Please specify the ID of the Character you'd like to add!**")
        return

    if not permission_check(ctx.author.id):
        return

    db_connection = WaifuLaifu(server=ctx.guild, user=ctx.author)
    result, name = db_connection.get_character_data(int(character_id), site)
    if result:
        await send_message(ctx.author, f"**Character {name} successfully added!**")
    else:
        await send_message(ctx.author, f"**Character with ID {character_id} already exists! Possible conflict due to the usage of multiple Import Sites?**")


@client.command()
async def say(ctx, channel=None):
    """
    Make the Bot say X. Admin only.
    :param ctx: Invocation Message Data. Includes Message.
    :param channel: Channel the Message should be sent into.
    """
    await delete_command(ctx.message)
    if not permission_check(ctx.author.id):
        return

    if channel is not None:
        channel = client.get_channel(int(re.sub("\D", "", channel)))
    if type(channel) is not discord.TextChannel:
        channel = ctx.guild.default_channel
    message = ctx.message.content.replace(f"{client.command_prefix}say ", "").replace(f"{channel.mention} ", "")
    async with channel.typing():
        await asyncio.sleep(3)
        await send_message(channel, message)
    gen_log(f"{ctx.author.id} used {client.command_prefix}say. Message: {message}")


@client.command()
async def pause(ctx):
    """
    Pauses the Bot making most commands unavailable. Admin only.
    """
    global paused
    await delete_command(ctx.message)
    if not permission_check(ctx.author.id):
        return

    if paused:
        paused = False
        await send_message(ctx.channel, "**Bot has been unpaused.**")
        await client.change_presence(activity=discord.Game(name=config_data['General']['PlayingStatus']))
    else:
        paused = True
        await send_message(ctx.channel, "**Bot has been paused. Remember to unpause it!**")
        await client.change_presence(activity=discord.Game(name="Under Maintenance!"))


##################################
#       SCHEDULED ACTIONS        #
##################################

scheduler = AsyncIOScheduler()


@client.command()
async def update(ctx):
    """
    Checks for Updates. Admin only.
    :param ctx: Invocation Message Data.
    """
    await delete_command(ctx.message)
    if not permission_check(ctx.author.id):
        return

    await ctx.send("Checking for Updates...")
    await schedule_update(ctx)


async def schedule_update(ctx=None):
    gen_log("Checking for Updates...")
    result = subprocess.check_output("git pull", shell=True).decode("UTF-8")
    gen_log(result)
    if result == "Already up to date.":
        return

    if ctx is not None:
        await ctx.send("Rebooting...")
    gen_log("Rebooting...")
    sys.exit(0)


@client.command()
async def backup(ctx):
    """
    Starts a Backup. Admin only.
    :param ctx: Invocation Message Data.
    """
    await delete_command(ctx.message)
    if not permission_check(ctx.author.id):
        return

    await schedule_database_backup()
    await ctx.send("Backup complete!")
    gen_log(f"User {ctx.author.id} forced a Backup.")


async def schedule_database_backup():
    timer_start = time.time()
    if not os.path.exists(str(Path("./backups"))):
        os.makedirs(str(Path("./backups")))
    call(db_backup_stuff)
    call(["zip", "-r", str(Path(f"./backups/Untitled-Backup-{datetime.datetime.now().strftime('%Y-%m-%d')}.zip")), str(Path(f"./backups/{datetime.datetime.now().strftime('%Y-%m-%d')}"))])
    timer_end = time.time()
    db_log(message="Daily Database Backup completed!", processing_time=round(timer_end - timer_start, 2))


scheduler.add_job(schedule_database_backup, 'interval', start_date=parser.parse('00:00', tzinfos={'GMT': tzutc()}), hours=int(config_data['General']['BackupTime']), name="Daily Database Backup")
scheduler.add_job(schedule_update, 'interval', start_date=parser.parse('12:00', tzinfos={'GMT': tzutc()}), hours=int(config_data['General']['UpdateTime']), name="Daily Update Check")
scheduler.add_job(refresh_shop, 'interval', start_date=parser.parse('00:00', tzinfos={'GMT': tzutc()}), hours=int(config_data['General']['ShopRefreshTime']), name="Daily Shop Refresh")

##################################

exempt_commands = ["pause", "update", "backup"]


@client.event
async def on_message(message):
    if (not paused or message.content.split(" ")[0][1:] in exempt_commands) and not message.author.bot:
        db_connection = WaifuLaifu(server=message.guild, user=message.author)
        if db_connection.server_document is not None:
            if message.content.startswith(db_connection.server_document['settings']['command_prefix']):
                client.command_prefix = db_connection.server_document['settings']['command_prefix']
                await client.process_commands(message)
            elif (random.uniform(0, 1) <= 0.05) or (db_connection.user_document['premium'] > 0 and random.uniform(0, 1) <= 0.1):
                await spawn_character(message)
        elif db_connection.server_document is None and message.content.startswith("."):
            client.command_prefix = "."
            await client.process_commands(message)
        db_connection.close(override=True)


@client.event
async def on_command_error(ctx, error):
    if type(error) is (discord.ext.commands.CommandNotFound or discord.ext.commands.MissingRequiredArgument or discord.ext.commands.BadArgument):
        return

    gen_log(f"[ERROR] {error}")
    for user_id in dev_list:
        user = client.get_user(user_id)
        await send_message(user, f"[ERROR] {error}")


##################################

@client.command()
async def woosh(ctx):
    await spawn_character(ctx)

if __name__ == '__main__':
    client.run(bot_key)
