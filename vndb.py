import json
import ssl
import socket


class VNDB:

    def __init__(self, username, password):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.data_buffer = bytes(1024)
        self.clientvars = {'protocol': 1, 'clientver': 0.1, 'client': 'VNDB_Python'}
        self.login_data = [username, password]
        self.sslcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        self.sslcontext.verify_mode = ssl.CERT_REQUIRED
        self.sslcontext.check_hostname = True
        self.sslcontext.load_default_certs()
        self.loggedin = False
        self.sslwrap = self.sslcontext.wrap_socket(self.socket, server_hostname="api.vndb.org")
        self.sslwrap.connect(("api.vndb.org", 19535))
        self.login()

    def login(self):
        result = self.send_command('login ' + json.dumps(self.clientvars))
        if result is "ok":
            self.loggedin = True

    def send_command(self, command):
        command += '\x04'
        self.sslwrap.sendall(command.encode('utf-8'))
        return self._receive_data()

    def _receive_data(self):
        temp = ""
        while True:
            self.data_buffer = self.sslwrap.recv(1024)
            if '\x04' in self.data_buffer.decode('utf-8', 'ignore'):
                temp += self.data_buffer.decode('utf-8', 'ignore')
                break
            else:
                temp += self.data_buffer.decode('utf-8', 'ignore')
                self.data_buffer = bytes(1024)
        temp = temp.replace('\x04', '')

        if temp == "ok":
            return "ok"
        else:
            return json.loads(temp.split(' ', 1)[1])

    def get_novel(self, novel_id):
        data = self.send_command(f'get vn basic,details,anime,relations,tags (id={int(novel_id)}) ' + json.dumps({'results': 25}))
        return data

    def get_character(self, character_id=None):
        if character_id is not None:
            data = self.send_command(f'get character basic,details,meas,traits,vns,instances (id={int(character_id)}) ' + json.dumps({'results': 25}))
            return data
        return None

    def get_characters(self, novel_id=None):
        if novel_id is not None:
            data = self.send_command(f'get character basic,details,meas,traits,vns,instances (vn={int(novel_id)}) ' + json.dumps({'results': 25}))
            return data
        return None

    def close(self):
        self.sslwrap.close()
